﻿

*********************************************************************

README FOR IPC program
*********************************************************************
Team Members:
----------------
VIVEK VISWANATHAN(A20294994)
KINNERA RAVULAPALLI(A20326163)
SAHITYA VANTIPALLI(A20326237)


Minix Version : 3.2.1 
Compiler : clang 


Loading Program to minix:
------------------------------

1. use the below commands to configure ssh
	pkgin update
	pkgin install openssh
2. Set the password for user by typing passwd
3. Use an ftp client like fileZilla to connect to MINIX using ssh and transfer the file 
4. Naviagate to /Project2/
5. Run TestIpc to run the program of clanf TestIpc.c -o TestIpc to rebuild and run the single app version
6. Run sh Run_parallel.sh to run the mutiple publisher/subscriber program. Please note that this program takes 2 minutes to terminate.
7. Source code is /usr/src/servers/pm folder
8. A copy of all sources is given in Source Code folder.
9. Document folder would contain the design document and test cases
