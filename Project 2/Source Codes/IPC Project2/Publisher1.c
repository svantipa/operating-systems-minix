#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <fcntl.h>
#include <unistd.h>
#include<sys/wait.h>
#include "ErrorCodes.h"
#include <lib.h>

#define p_Sprocessid m1_i3 

message m;

int createTopicGroup(char *msg, pid_t SprocessId){

  //message m;
  strncpy(m.m3_ca1,msg,M3_STRING);
  m.p_Sprocessid=SprocessId;

  return(_syscall(PM_PROC_NR, PM_CREATETOPICGROUP, &m));

}

int publishData(char *msg,pid_t SprocessId){
	
	strncpy(m.m3_ca1,msg,M3_STRING);
	m.p_Sprocessid=SprocessId;
	return(_syscall(PM_PROC_NR, PM_PUBLISHERSENDMESSAGE, &m));
	
	
}

int createTopicPublisher(char *msg,pid_t SprocessId){
	printf("App: Create publisher qroup\n");
	
	strncpy(m.m3_ca1,msg,M3_STRING);
	m.p_Sprocessid=SprocessId;
	return(_syscall(PM_PROC_NR, PM_CREATEPUBLISHER, &m));
	
}

int main(){
    int option=0;
    int select=0;
    int mbId;
    char *name;
    int pid;
    int i=0;
	
	char sampleText[]="Movies";
	char buffer[100];
	
	printf("App:*****************Publisher1 **********************\n");
	
	printf("App:Creating topic group\n");
	createTopicGroup(sampleText,getpid());
	
	printf("App:Creating Publisher group\n");
	createTopicPublisher(sampleText,getpid());

		do{
		buffer[i] = sampleText[i];
		i++;
	}while(sampleText[i] != '\0');
	
	strcat(buffer,",Avengers");
	printf("App: Publisher 1 publish Data\n");
	publishData(buffer,getpid());
	printf("App: Publisher 1 sleep for 5 seconds\n");
	sleep(5);
	
	buffer[6]='\0';
	buffer[7]='\0';
	
	strcat(buffer,",TopGun");
	printf("App: Publisher 1 publish Data\n");
	publishData(buffer,getpid());
	printf("App: Publisher 1 sleep for 5 seconds\n");
	sleep(5);
	
	buffer[6]='\0';
	buffer[7]='\0';
	
	strcat(buffer,",Bashaa");
	printf("App: Publisher 1 publish Data\n");
	publishData(buffer,getpid());
	printf("App: Publisher 1 sleep for 5 seconds\n");
	sleep(5);
	
	buffer[6]='\0';
	buffer[7]='\0';
	
	strcat(buffer,",Biilla");
	printf("App: Publisher 1 publish Data\n");
	publishData(buffer,getpid());
	printf("App: Publisher 1 sleep for 5 seconds\n");
	sleep(5);
	
	buffer[6]='\0';
	buffer[7]='\0';
	
	strcat(buffer,",StarTK");
	printf("App: Publisher 1 publish Data\n");
	publishData(buffer,getpid());
	printf("App: Publisher 1 sleep for 5 seconds\n");
	sleep(5);
	
	buffer[6]='\0';
	buffer[7]='\0';
	
	strcat(buffer,",Starwars");
	printf("App: Publisher 1 publish Data\n");
	publishData(buffer,getpid());
	printf("App: Publisher 1 sleep for 5 seconds\n");
	sleep(5);
	
	printf("Publisher1: Finished publishing\n");
}
	