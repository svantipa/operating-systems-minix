#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <fcntl.h>
#include <unistd.h>
#include<sys/wait.h>
#include "ErrorCodes.h"
#include <lib.h>

#define p_Sprocessid m1_i3 

message m;

int retriveData(char *msg,pid_t SprocessId){
	
	strncpy(m.m3_ca1,msg,M3_STRING);
	m.p_Sprocessid=SprocessId;
	return(_syscall(PM_PROC_NR, PM_SUBSCRIVERRECEIVEMESSAGE, &m));
		
}


int createTopicSubscriber(char *msg,pid_t SprocessId){
	//printf("App: Create subscriber qroup\n");
	
	strncpy(m.m3_ca1,msg,M3_STRING);
	m.p_Sprocessid=SprocessId;
	return(_syscall(PM_PROC_NR, PM_CREATESUBSCRIVER, &m));
	
}

int main(){
	
	char sampleText[]="Movies";
	int count=0;
	
	printf("App:************Subscribers 2 read *************** \n");
		
			printf("App:Creating Subscriber2\n");
			createTopicSubscriber(sampleText,getpid());

		
	while(count !=60){
			if((retriveData(sampleText,getpid())==EC_NO_MESSAGEGROUP ))
				printf("Subscriber2: No pending messages. Let us try after 1 second\n");
			else	
				printf("Subscriber2: Message received\n");
			
			sleep(1);
			count++;
	}

	printf("Subscriver2: Exiting\n");
	
}