#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <fcntl.h>
#include <unistd.h>
#include<sys/wait.h>
#include "ErrorCodes.h"
#include <lib.h>

#define p_Sprocessid m1_i3 

message m;

int createTopicGroup(char *msg, pid_t SprocessId){

  //message m;
  strncpy(m.m3_ca1,msg,M3_STRING);
  m.p_Sprocessid=SprocessId;

  return(_syscall(PM_PROC_NR, PM_CREATETOPICGROUP, &m));

}

int gettopics(pid_t SprocessId){
	printf("App: Publish topics in qroup\n");
	m.p_Sprocessid=SprocessId;
    _syscall(PM_PROC_NR, PM_LOOKTOPICGROUP, &m);
	printf("Message=%s, retval=%x",m.m3_ca1,m.m_type);
	return 0;
}

int createTopicPublisher(char *msg,pid_t SprocessId){
	printf("App: Create publisher qroup\n");
	
	strncpy(m.m3_ca1,msg,M3_STRING);
	m.p_Sprocessid=SprocessId;
	return(_syscall(PM_PROC_NR, PM_CREATEPUBLISHER, &m));
	
}

int createTopicSubscriber(char *msg,pid_t SprocessId){
	printf("App: Create subscriber qroup\n");
	
	strncpy(m.m3_ca1,msg,M3_STRING);
	m.p_Sprocessid=SprocessId;
	return(_syscall(PM_PROC_NR, PM_CREATESUBSCRIVER, &m));
	
}

int publishData(char *msg,pid_t SprocessId){
	
	strncpy(m.m3_ca1,msg,M3_STRING);
	m.p_Sprocessid=SprocessId;
	return(_syscall(PM_PROC_NR, PM_PUBLISHERSENDMESSAGE, &m));
	
	
}

int retriveData(char *msg,pid_t SprocessId){
	
	strncpy(m.m3_ca1,msg,M3_STRING);
	m.p_Sprocessid=SprocessId;
	return(_syscall(PM_PROC_NR, PM_SUBSCRIVERRECEIVEMESSAGE, &m));
		
}


int main(){
    int option=0;
    int select=0;
    int mbId;
    char *name;
    int pid;
    	
	char sampleText[]="Movies";
	char sampleText1[]="Music";
	char sampleText2[]="Movies";
	char sampleText3[]="Movies";
	
	printf("App:*****************Single process Test App **********************\n");
	printf("App:Creating topic group\n");
	createTopicGroup(sampleText, getpid());
	createTopicGroup(sampleText1, getpid());
	gettopics(getpid());
	
	printf("App:Creating Publisher group\n");
	createTopicPublisher(sampleText,getpid());

	printf("App:Creating Subscriver\n");
	createTopicSubscriber(sampleText,getpid());
	
	strcat(sampleText,",Avengers");
	printf("App: Publish Data\n");
	publishData(sampleText,getpid());
	strcat(sampleText3,",TopGun");
	publishData(sampleText3,getpid());
	
	printf("App: Retrive Data\n");
	retriveData(sampleText2,getpid());
	retriveData(sampleText2,getpid());
	
	return 0;
}