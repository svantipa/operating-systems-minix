/*        Custom IPC for Pulblisher-Subscriber  model
*				Project Group 13
*			  CS551- Advanced Operating systems
*
*/

#include "pm.h"
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <lib.h>
#include "proto.h"
#include "ErrorCodes.h"

struct topicGroups{
	int pidCreator;
	char messageText[100];	
	int pidPublisher;
	int pidSubscribers[10];
	int subscribersCount;
	int messageCount;
	int subscribersRead[5][10][1];
	char messages[5][100];

};

/*Messages Definitions*/

#define p_Sprocessid m1_i3    /*For Sender Process*/

struct topicGroups mytopicgroups[100]; //Limit to max of 100 groups
int currentCount=0;


int do_lookuptopicgroup(){
	int i=0;	
	for(i=0;i<currentCount;i++){
		printf("Kernel: Current topics of interest =%s, creatorPID=%x\n",mytopicgroups[i].messageText,mytopicgroups[i].pidCreator);
	}		
	
	return EC_OK;
}

static readSubscriberInProgress=0;

//Called by subscribers
int do_retriveData(){
	pid_t SenderprocessId;
	int i=0,j=0,messageLength=0,subscriberID=0,subscriberPlaceholder=0;
	//int messagesCount[5]; //One subscriber can have a max of 5 messages not read
	//int interestGroups[100]; //One subscrivber can have max of 100 intrest groups
	char tempBuffer[1000];
	int countValue=0;
	
	if(readSubscriberInProgress)
			return EC_SUBSCRIBERBUSY;
	
	readSubscriberInProgress=1;	
	//Get the process ID
    SenderprocessId=m_in.p_Sprocessid;
	
	//Copy the message
	messageLength = M3_STRING-1;
    strncpy(tempBuffer, m_in.m3_ca1, messageLength);

	for(i=0;i<currentCount;i++){
		if(!(strncmp(mytopicgroups[i].messageText,tempBuffer,messageLength))){
			printf("Kernel:: Interest group exists..checking for subscribers\n");
			for(j=0;j<10; j++){
				if(mytopicgroups[i].pidSubscribers[j] == SenderprocessId){
					printf("Kernel:: Subscriber is registered with the group PID=%x\n", SenderprocessId);
					messageLength=1;
					subscriberID=j;
					break;
				}
			}
			countValue=i;
			break;
		}		
	}
	
	if(!messageLength){
		printf("Subscriber does not exist in the message group\n");
		readSubscriberInProgress=0;
		return EC_NOSUBSCRIBERINGROUP ;
	}
	
	
	//Number of messages not read
	for(i=0;i<mytopicgroups[countValue].messageCount;i++){
		printf("Kernel: Subscriber read status=%d\n",mytopicgroups[countValue].subscribersRead[i][subscriberID][0]);
	if(mytopicgroups[countValue].subscribersRead[i][subscriberID][0] == 1){
		printf("Kernel: Subscriber  PID=%x, Interest Group message=%s \n",SenderprocessId,mytopicgroups[countValue].messages[i]);
		mytopicgroups[countValue].subscribersRead[i][subscriberID][0]=0; //Mark as read
	}
	else	{
		printf("Subscriber has no new messages in Interest group\n");
		subscriberPlaceholder=1;
	}
}

	//If everyone has read the message.Then remove the same.
	for(i=0;i<10;i++){
			if(mytopicgroups[countValue].subscribersRead[mytopicgroups[countValue].messageCount][i][0]==1){
				//printf("There is further unread subscribers");
				break;
			}
			mytopicgroups[countValue].messageCount--;
	}
	
	readSubscriberInProgress=0;
	
if(subscriberPlaceholder)
	return EC_NIL;
else	
	return EC_OK;
}
	

static readInProgress=0;

int do_receiveData(){
	pid_t SenderprocessId;
	int i=0,j=0,messageLength=0;
	char tempBuffer[1000];
	char interestGroup[500];
	char messageReceived[500];
	int countValue=0;

	if(readInProgress !=0){
		printf("Kernel: Producer Busy.Please try after some time");
		return EC_PRODUCERBUSY;
	}
	readInProgress=1;
	//Get the process ID
    SenderprocessId=m_in.p_Sprocessid;

	//Copy the message
	messageLength = M3_STRING-1;
    strncpy(tempBuffer, m_in.m3_ca1, messageLength);

	printf("Kernel: Publisher  pid=%x, Message=%s\n",SenderprocessId, tempBuffer );
	
	//No strtok in kernel space. Use conventional method
	//Get the interest group
	while(tempBuffer[i] != ','){
		interestGroup[i] = tempBuffer[i];	
		i++; 	
	}
	
	//Now get the message
	i++;
	while(tempBuffer[i] != '\0'){
		messageReceived[j]=tempBuffer[i];
		i++;
		j++;
	}
	
	
	printf("Kernel: Interest group=%s, Message received =%s\n", interestGroup,messageReceived);
	
	//Loop to check if message exists
	for(i=0;i<currentCount;i++){
		if(!(strncmp(mytopicgroups[i].messageText,interestGroup,messageLength))){
			//printf("Process group exists\n");
			messageLength=1;
			countValue=i;
			break;
		}
	}
	
	//If the topic of interest exists
	if(messageLength){
		if(mytopicgroups[countValue].messageCount <5){
			strcpy(mytopicgroups[countValue].messages[mytopicgroups[countValue].messageCount],messageReceived);
			printf("Kernel: Received message=%s\n",mytopicgroups[countValue].messages[mytopicgroups[countValue].messageCount]);
			//Make all subscribers as unread
			for(i=0;i<10;i++)
				mytopicgroups[countValue].subscribersRead[mytopicgroups[countValue].messageCount][i][0]=1;
			mytopicgroups[countValue].messageCount++;
		}else{
			printf("KErnel: Message buffer already full.Cannot send anymore till everyone finishes reads\n");
			return EC_QUEUENOTEMPTY;
		}	
	}else {
		printf("Kernel: The request %s interest group does not exist\n",interestGroup);    
		readInProgress=0;
		return EC_NO_MESSAGEGROUP;
	}	
	//make producer free
	readInProgress=0;
	return EC_OK;

}



int do_topicSubscriber(){
	pid_t SenderprocessId;
	int i=0,messageLength=0;
	char tempBuffer[1000];

		//Get the process ID
    SenderprocessId=m_in.p_Sprocessid;

	//Copy the message
	messageLength = M3_STRING-1;
    strncpy(tempBuffer, m_in.m3_ca1, messageLength);

	printf("Kernel: Subscriber  pid=%x, Interest group=%s\n",SenderprocessId, tempBuffer);
	
	//Loop to check if message exists
	for(i=0;i<currentCount;i++){
		if(!(strncmp(mytopicgroups[i].messageText,tempBuffer,messageLength))){
			//printf("Process group exists\n");
			messageLength=1;
			break;
		}
	}

	//Check if the message exists
	if(messageLength){
			printf("Kernel: Interest group exists..adding Subscriber\n");
			if(mytopicgroups[messageLength].subscribersCount <10){
			mytopicgroups[messageLength].pidSubscribers[mytopicgroups[messageLength].subscribersCount] = SenderprocessId;
			mytopicgroups[messageLength].subscribersCount++;
			}else{
				printf("Interest group has max Subscriber. Please remove any one to add new\n");
				return EC_FULL_SUBSCRIBERS;
			}
	}
 	else{
		printf("Kernel: Interest group does not exist\n");
		return EC_NO_MESSAGEGROUP;
	}	
	
	readInProgress=0;
	return EC_OK;
	
}

int do_topicPublisher(){
	pid_t SenderprocessId;
	int i=0,messageLength=0;
	char tempBuffer[1000];
		
	//Get the process ID
    SenderprocessId=m_in.p_Sprocessid;

	//Copy the message
	messageLength = M3_STRING-1;
    strncpy(tempBuffer, m_in.m3_ca1, messageLength);

	printf("Kernel: Register publisher pid=%x, Intrestgroup=%s\n",SenderprocessId, tempBuffer );
		//Loop to check if message exists
	for(i=0;i<currentCount;i++){
		if(!(strncmp(mytopicgroups[i].messageText,tempBuffer,messageLength))){
			//printf("Process group exists\n");
			messageLength=1;
			break;
		}
	}
	
	//Check if the message exists
	if(messageLength){
		printf("Kernel: Interest group exists..check if there is no publisher\n");
		if(mytopicgroups[messageLength].pidPublisher != 0){
			printf("There is already a publisher for interest group %s\n", mytopicgroups[messageLength].messageText);
			return EC_PUBLISHER_EXISTS;
		}
		else{
			mytopicgroups[messageLength].pidPublisher = SenderprocessId;
			printf("Kernel: Registering a new interest group %s publisher with PID=%x\n", mytopicgroups[messageLength].messageText,mytopicgroups[messageLength].pidPublisher);
		}			
	}
	else{
		printf("Kernel: Interest group does not exist\n");
		return EC_NO_MESSAGEGROUP;
	}	
	return EC_OK;
}

int do_createtopicgroup(){
	pid_t SenderprocessId;
    int i=0,messageLength,messageExists=0;
	char tempBuffer[1000];

	//Get the process ID
    SenderprocessId=m_in.p_Sprocessid;

	//Copy the message
	messageLength = M3_STRING-1;
    strncpy(tempBuffer, m_in.m3_ca1, messageLength);
	
	printf("Kernel: Create topic group:message =%s, messagelength=%x\n",tempBuffer,messageLength);
	
	//Loop to check if message exists
	for(i=0;i<currentCount;i++){
		if(!(strncmp(mytopicgroups[i].messageText,tempBuffer,messageLength))){
			messageExists=1;
			printf("Kernel: Request topic of interest exists =%s\n",mytopicgroups[i].messageText);
			break;
		}			
	}
	
	if(messageExists){
		//return
		printf("Kernel: Request topic of interest exists..Returning\n");
		return EC_MESSAGE_GROUP_EXISTS;
	}
		
	printf("Kernel: Topic group ID %d, senderPID=%x, topic name=%s\n",currentCount,SenderprocessId,tempBuffer);
	//Copy the process ID and message
	mytopicgroups[currentCount].pidCreator = SenderprocessId;
	strncpy(mytopicgroups[currentCount].messageText, tempBuffer,messageLength);

	//Increment count	
	currentCount++;
	
	return EC_OK;
}

