#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <setjmp.h> 
#include <minix/minlib.h>

/******************************************************************************
 *                                Defines                                     *
******************************************************************************/
#define FALSE						           0
#define TRUE						           1
#define COMMAND_LENGTH  			     512
#define TOKEN_SIZE			           64
#define MAXIMUM_TOKENS			       COMMAND_LENGTH/TOKEN_SIZE
#define STANDARD_PIPE       -1


typedef struct
{
	int argc;
	char **argv;
     
}CommandStruct;

typedef struct {
	char prompt[TOKEN_SIZE];
}ctrlHandler;

typedef struct history
{
   char *pLine;
   char *pCommand;
   char *pAlias;
   char *pAliasCmd;
   int  iID;
   struct history *pNextAddress;
}history;

char my_home[50],my_prompt[10],my_src_path[10][50],buffer[100];
int debug=0;
jmp_buf jump_buffer;
history *pStart = NULL, *pEnd = NULL;

char* malloc_char(int nBytes);
void Initialization(void);
void Read_mysh_profile(void);
int executeNormal(CommandStruct* CommandLine);
int processCommand(CommandStruct* CommandLine);
int executeCD(CommandStruct* CommandLine);
CommandStruct* parseLine(char *str);
int executePipe(CommandStruct* CommandLine,CommandStruct* CommandLine_P);
int executeFre(CommandStruct* CommandLine,CommandStruct* CommandLine_F);
/*signal(SIGQUIT, ctrl_CHandler);*/
/*setjmp(jump_buffer);*/

/******************************************************************************
 * Function         : spawnProcess
 *
 * Description      : This section of the code spawns a new process and
 *                    returns the process id.
 *
 * Input Argument   : int y
 *
 * Output Argument  : None
 *
 * Return Value     : None
 *****************************************************************************/

spawnProcess(int y)
{
   int pid;
   printf("y is : %d\n", y);
	 if((pid=getpid())<0)
		  perror("Not able to return the process id");
	 else
	    printf("\nThe program's process Id is : %d\n",pid);  
}

/* You can remove this code if yours is working correctly*/


/******************************************************************************
 * Function         : ctrl_CHandler
 *
 * Description      : This section of code implements the signal handler. When
 *                    When we press ctrl-c it asks the user whether he wants
 *                    to exit or not.
 *
 * Input Argument   : int param
 *
 * Output Argument  : None
 *
 * Return Value     : None
 *****************************************************************************/
/* void ctrl_CHandler(int param)
{
   char alter[TOKEN_SIZE];
       
   printf("\n Exit from the shell: Are you sure? [yes/no]: ");
   scanf("%31s", alter);
       
   if((strcmp(alter, "y") == 0) || (strcmp(alter, "Y") == 0) || (strcmp(alter, "yes") == 0)
   || (strcmp(alter, "YES") == 0))
      exit(0);
   else
      signal(SIGQUIT, ctrl_CHandler);
   longjmp(jump_buffer,1);
}*/




/******************************************************************************
 * Function         : get_history
 *
 * Description      : This section of the code spawns a new process and
 *                    returns the process id.
 *
 * Input Argument   : char *name
 *                    int  strLen
 *                    int  iID
 *
 * Output Argument  : None
 *
 * Return Value     : None
 *****************************************************************************/
void get_history(char *name, int strLen, int iID)
{
   char    *tokens[MAXIMUM_TOKENS];
   history *pCurrent = NULL, *pTempCurrent = NULL;
   int     iLen = 0;

   pCurrent = (history *)malloc(sizeof(history));
   pCurrent->pNextAddress = NULL;

   pCurrent->pLine = (char *)malloc (strLen * sizeof(char));
   strcpy ((pCurrent->pLine), name);

 /*  getTokens(name, tokens);*/

   iLen = strlen (tokens[0]);
   pCurrent->pCommand = (char *)malloc(iLen*sizeof(char));
   strcpy ((pCurrent->pCommand), tokens[0]);

   if ((strcmp(tokens[0],"Alias") == 0) || (strcmp(tokens[0],"alias")==0))
   {
      iLen = strlen (tokens[1]);

      pCurrent->pAlias = (char *)malloc(iLen*sizeof(char));
      strcpy((pCurrent->pAlias),tokens[1]);

      iLen = strlen (tokens[3]);
      pCurrent->pAliasCmd = (char *)malloc(iLen*sizeof(char));
      strcpy((pCurrent->pAliasCmd),tokens[3]);
   }
   else
   {
      pCurrent->pAlias = NULL;
      pCurrent->pAliasCmd = NULL;
   }
   
   pCurrent->iID = iID;

   if ((NULL == pStart) && (NULL == pEnd))
   {
      pEnd = pStart = pCurrent;
   }
   else
   {
      pEnd->pNextAddress = pCurrent;
      pEnd = pCurrent;
   }

   pTempCurrent = pStart;
   while (NULL != pTempCurrent)
   {
      pTempCurrent = pTempCurrent->pNextAddress;      
   }
}


int main(void)
{
	
		int j,chk=0;
		char line[256];
		char *argList[]={"ls",NULL};
	
		CommandStruct* CommandLine = (CommandStruct*) malloc(sizeof(CommandStruct)); 
		
        system("clear");
        printf("********************************\n");
        printf("Welcome to Our Minix Shell Developed By:\n");
        printf("Rashmi       \n");
        printf("Varshali\n");
        printf("Abhiram\n");
        printf("********************************\n");

		Initialization();				/* initialize home directory, prompt, src directory*/
		Read_mysh_profile();			/* read profile file*/
		
		while(1)
		{
			CommandLine->argv = (char**)calloc(256, sizeof(char*)); 
			fflush(stdout);
			printf("%s",my_prompt);		/* print "mysh>"*/
				
		
				chk=processCommand(CommandLine);
			if (chk==2) exit(1);		/* if command == exit*/
		}
		return 0;
}

/* Read command line arguments, parse them and then call execute module*/

int processCommand(CommandStruct* CommandLine)
{
	int i,chk,pos_ex=0,pos_gt=0, flagP=0,flagFre=0, parallel = FALSE;
	char *pipe_str,*ex_str,*temp_str,*str,*fst_str;
	CommandStruct *CommandLine_P, *CommandLine_F;
	/*char *name;
    char* tokens[MAXIMUM_TOKENS];
    char* retu_token[MAXIMUM_TOKENS];
    char* tokens1[MAXIMUM_TOKENS];*/
   char pipe_command[COMMAND_LENGTH];

	fflush(stdin);	
	fgets(buffer, 100, stdin);
	if(strcmp(buffer,"\n")==0) return 0;
	strcpy(strchr(buffer, '\n'), "\0"); 
	
	/*name = (char *)malloc(COMMAND_LENGTH*sizeof(char));*/


	/* parse command line arguments */
	temp_str = strtok(buffer, ">");
	str=temp_str;
	temp_str = strtok(NULL, ">");
	if(temp_str!=NULL)
	{
		flagFre=1;
		ex_str=temp_str;
		CommandLine_F = parseLine(ex_str);
		/*printf(" > = %s, %s, %s\n",ex_str, CommandLine_F->argv[0],CommandLine_F->argv[1]);*/
	}
	
	      /*getTokens(name,tokens);*/
	temp_str = strtok(str, "|");
	fst_str=temp_str;
	temp_str = strtok(NULL, "|");
	if(temp_str!=NULL)
	{
		flagP=1;
		pipe_str=temp_str;
		CommandLine_P = parseLine(pipe_str);
	}
	
	CommandLine = parseLine(fst_str);
	if(strcmp(CommandLine->argv[0],"exit")==0)
	{
		exit(1);
		return 2;
	}
	
	
	/* execute command */
	if (flagP==1)
	{
		executePipe(CommandLine,CommandLine_P);
	}
	else if (flagFre==1)
	{
		executeFre(CommandLine,CommandLine_F);
	}
	else if ((strcmp(CommandLine->argv[0],"cd")==0)||(strcmp(CommandLine->argv[0],"chdir")==0))
	{
		chk=executeCD(CommandLine);
	}
      else if ((strcmp(CommandLine->argv[0],"getpid")==0)||(strcmp(CommandLine->argv[0],"GETPID")==0))
	{
		spawnProcess(1);
	}
	/* This is for the Integer expresison evaluation. I am parsing the cmd line data, but need to store it in the variable*/
	else if ((strcmp(CommandLine->argv[1], "=") == 0) && (strcmp(CommandLine->argv[0],"PROMPT")!=0)&&(strcmp(CommandLine->argv[0],"HOME")!=0)&&(strcmp(CommandLine->argv[0],"home")!=0)&&(strcmp(CommandLine->argv[0],"prompt")!=0))
         {  
		for(i =0; i<=CommandLine->argc; i++) {
			    
		    	printf("test first : %s \n",CommandLine->argv[i]);

				
		}

					/* Call for the function that stores the values of variables. We also need to 
					* store the value of the expression to be passed to the calc.c program */
            /*intgrExpr(CommandLine->argv);*/
 	     	 } 
         else if ((strcmp(CommandLine->argv[1], "=") == 0) && (strcmp(CommandLine->argv[4], "=") == 0 ) && (strcmp(CommandLine->argv[0], "=") == 0 )&& (strcmp(CommandLine->argv[0],"PROMPT")!=0)&&(strcmp(CommandLine->argv[0],"HOME")!=0)&&(strcmp(CommandLine->argv[0],"home")!=0)&&(strcmp(CommandLine->argv[0],"prompt")!=0))
         {     
			 	printf("test secnd\n");
/*            intgrExpr(CommandLine->argv);*/
 	     	 }
	
	else
	{
		executeNormal(CommandLine);
	}
	return 1;
}

CommandStruct* parseLine(char *str)
{
	int i;
	char *temp_str;
	CommandStruct* CommandLine_temp = (CommandStruct*) malloc(sizeof(CommandStruct)); 
	CommandLine_temp->argv = (char**)calloc(256, sizeof(char*)); 
	CommandLine_temp->argv[0] = strtok(str, " ");
	i=1;
	while(1)
	{
			temp_str=strtok(NULL," ");
			CommandLine_temp->argv[i]=temp_str;
			if(CommandLine_temp->argv[i]==NULL)
			break;
			i++;
	}
	/*printf("parse > =%s %s\n",CommandLine_temp->argv[0],CommandLine_temp->argv[1]);*/
	CommandLine_temp->argc=i;
	return CommandLine_temp;
}

int executeFre(CommandStruct* CommandLine,CommandStruct* CommandLine_F)
{
		int status;
		char *command=malloc_char(16);
		pid_t pid;
		
		strcpy(command,CommandLine->argv[0]);
		pid=fork();
		if (pid!=0)
		{
			wait(&status);
		}
		else
		{	
				freopen(CommandLine_F->argv[0],"w",stdout);
				execvp(command,CommandLine->argv);
		}
		
		return 1;
}


int executePipe(CommandStruct* CommandLine,CommandStruct* CommandLine_P)
{
	int fd[2],fd_out,status;
	pid_t pid, pid2;

	pipe(fd);
	if ((pid=fork()) < 0) 
	{
		printf("Error: Fork failed\n");
		return 1;
	}
	if(pid==0)
	{
		close(fd[1]);
		dup2(fd[0],0);
		close(fd[0]);
		execvp(CommandLine_P->argv[0], CommandLine_P->argv); 
		printf("Error: execv failed\n");
		return 1;
	}
	else 
	{
		if((pid2=fork())< 0)
		{
			printf("Error: Fork failed\n");
			return 1;
		}
		if(pid2==0)
		{
			close(fd[0]);
			dup2(fd[1],1);
			close(fd[1]);
			execvp(CommandLine->argv[0], CommandLine->argv);
			printf("Error: execv failed\n");
			return 1;
		}
		else
		{
			close(fd[0]);
			close(fd[1]);
			while (wait(&status) != pid2);
		}
	}
	
	return 1;
}

/* Execute cd , chdir commands*/
int executeCD(CommandStruct* CommandLine)
{
	if(CommandLine->argc==2)
		chdir(CommandLine->argv[1]);
	else if(CommandLine->argc==1)
		chdir(my_home);
	else
	{
		printf("%s: too many arguments\n",CommandLine->argv[0]);
	}
	return 1;
}

/* Execute commands with or without argument such as: cat, ls*/

int executeNormal(CommandStruct* CommandLine)
{
		
		int status;
		char *command=malloc_char(16);
		pid_t pid;
		
		strcpy(command,CommandLine->argv[0]);
		pid=fork();
		if (pid!=0)
		{
			wait(&status);
		}
		else
		{	
			execvp(command,CommandLine->argv);
		}
		
		return 1;
}


/* Read_mysh_profile(); */
void Read_mysh_profile(void)
{
		char file_name[15],line_string[100];
		char word[10];
		FILE* file_handler;
		strcpy(file_name,".mysh_profile");	
		file_handler = fopen (file_name, "r");
				
		if (file_handler != NULL)
		{
			if(debug==1) printf("file opened\n");
			while(fgets(line_string,100,file_handler)!=NULL)
			{
				if(debug==1) printf("\nline_string=%s",line_string);
				strcpy(word,strtok(line_string," ;"));
				if(debug==1) printf("wordlist=%s\n",word);
				if(strcmp(word,"HOME")==0)
				{
					strcpy(my_home,strtok(NULL,"\n"));
				}
				else if(strcmp(word,"PROMPT")==0)
				{
					strcpy(my_prompt,strtok(NULL,"\n"));
				}
				else if(strcmp(word,"PATH")==0)
				{
					strcpy(my_src_path[0],strtok(NULL,";"));
					strcpy(my_src_path[1],strtok(NULL,"\n"));
				}

			} /* while (fgets!=NULL) */
				fclose(file_handler);
		} 
		else /* if(file_handler == NULL) */
		{
			if(debug==1) perror (file_name);
		}
		if(debug==1)
		{
			printf("HOME = %s\n",my_home);
			printf("PROMPT = %s\n",my_prompt);
			printf("PATH 1= %s\n",my_src_path[0]);
			printf("PATH 2= %s\n",my_src_path[1]);
		}
}

void Initialization(void)
{
	strcpy(my_home,"/");
	strcpy(my_prompt,"mysh>");
	strcpy(my_src_path[0],"/usr/bin");
	strcpy(my_src_path[1],"/bin");
}

char* malloc_char(int nBytes)
{	char *temp;
	temp = (char *) malloc(sizeof(char)*nBytes);
	return temp;
}