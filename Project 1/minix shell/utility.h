
/* Misc manifest constants */
#define MAXLINE    1024   /* max line size */
#define MAXARGS     128   /* max args on a command line */
#define MAXJOBS      16   /* max jobs at any point in time */
#define MAXJID    1<<16   /* max job ID */

/* Job states */
#define UNDEF 0 /* undefined */
#define FG 1    /* running in foreground */
#define BG 2    /* running in background */
#define ST 3    /* stopped */

struct job_t {              /* The job struct */
    pid_t pid;              /* job PID */
    int jid;                /* job ID [1, 2, ...] */
    int state;              /* UNDEF, BG, FG, or ST */
    char cmdline[MAXLINE];  /* command line */
};
typedef void handler_t(int);

void sigquit_handler(int sig);
void app_error(char *msg);
void unix_error(char *msg);
void usage(void);
extern struct job_t jobs[MAXJOBS];
void listjobs(struct job_t *jobs);
int pid2jid(pid_t pid);
struct job_t *getjobjid(struct job_t *jobs,int jid);
struct job_t *getjobpid(struct job_t *jobs,pid_t pid);
pid_t fgpid(struct job_t *jobs);
int deletejob(struct job_t *jobs,pid_t pid);
int addjob(struct job_t *jobs,pid_t pid,int state,char *cmdline);
int maxjid(struct job_t *jobs);
void initjobs(struct job_t *jobs);
void clearjob(struct job_t *job);
handler_t *Signal(int signum,handler_t *handler);
extern int verbose;
extern int nextjid;
void waitfg(pid_t pid);