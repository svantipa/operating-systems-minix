
/*
 *  System call wrappers to avoid code bloat while
 *  checking for errors
 */
#define _POSIX_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

pid_t Fork(void)
{   
    int pid=-1;
    if ((pid = fork())<0)
    {
        unix_error("fork error");
        exit(0);
    }
    return pid;
}

int Pipe(int p[]){
    int ret_val=-1;
    if ((ret_val = pipe(p))<0)
    {        
        unix_error("pipe error");
        exit(0);
    }
    return ret_val;
}

int Kill(pid_t pid, int sig)
{
    int ret_val=-1;
    if ((ret_val = kill(pid, sig))<0)
    {        
        unix_error("kill error");
        exit(0);
    }
    return ret_val;
}

/*This actually doesn't work in Minix*/
pid_t Setpgid(pid_t pid, pid_t pgid)
{
    pid_t ret_val=1;
    /*if ((ret_val = setpgid(pid, pgid))<0)
    {
        unix_error("setpgid error");
        exit(0);
    }*/
    return ret_val;
}


int Sigaddset(sigset_t *set, int signum)
{
    int is_ok;
    if((is_ok=sigaddset(set,signum))<0)
        unix_error("sigaddset error");
    return is_ok;

}

int Sigprocmask(int how, const sigset_t *set, sigset_t *oldset)
{
    int isok=1;
    if((isok=sigprocmask(how,set,oldset))<0)
    {unix_error("sigprocmask error");}
    return isok;

}

int Sigemptyset(sigset_t *set)
{
    int member;
    if((member=sigemptyset(set))<0)
        unix_error("sigemptyset error");
    return member;

}

pid_t Waitpid(pid_t pid, int *status, int options)
{
    pid_t pid_ret = waitpid(pid,status,options);

    if (pid_ret==-1)
    {
        unix_error("waitpid error");
        exit(0);
    }
    return pid_ret;
}
/* End of system call wrappers */