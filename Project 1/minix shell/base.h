pid_t Waitpid(pid_t pid,int *status,int options);
int Sigemptyset(sigset_t *set);
int Sigprocmask(int how,const sigset_t *set,sigset_t *oldset);
int Sigaddset(sigset_t *set,int signum);
pid_t Setpgid(pid_t pid,pid_t pgid);
int Kill(pid_t pid,int sig);
int Pipe(int p[]);
void unix_error(char *msg);
pid_t Fork(void);