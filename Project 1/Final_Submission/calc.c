#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#define MAX 10
#define EMPTY -1

int debugc = 0;

/* Program input would be like - 2+3*4+1 - Abhiram */

struct stack
{
	char data[MAX];
	int top;
};



int isempty(struct stack *s)
{
	return (s->top == EMPTY) ? 1 : 0;
}

void emptystack(struct stack* s)
{
	s->top=EMPTY;
}

void push(struct stack* s,int item)
{
	if(s->top == (MAX-1))
	{
		if(debugc) printf("\nSTACK FULL");
	}
	else
	{
		++s->top;
		s->data[s->top]=item;
	}
}

char pop(struct stack* s)
{
	char ret=(char)EMPTY;
	if(!isempty(s))
	{
		ret= s->data[s->top];
		--s->top;
	}
	return ret;
}

void display(struct stack s)
{
	while(s.top != EMPTY)
	{
		if(debugc) printf("\n%d",s.data[s.top]);
		s.top--;
	}
}

int isoperator(char e)
{
	if(e == '+' || e == '-' || e == '*' || e == '/' || e == '%')
		return 1;
	else
		return 0;
}


int priority(char e)
{
	int pri = 0;

	if(e == '*' || e == '/' || e =='%')
		pri = 2;
	else
	{
		if(e == '+' || e == '-')
			pri = 1;
	}
	return pri;
}

void parseExpr(char* infix, char * postfix, int insertspace)
{
	char *i,*p;
	struct stack X;
	char n1;
	emptystack(&X);
	i = &infix[0];
	p = &postfix[0];

	while(*i)
	{
		if(debugc) printf("i is : %s \n",i);
		if(debugc) printf("p is : %s \n",p);
		while(*i == ' ' || *i == '\t')
		{
			i++;
		}

		if( isdigit(*i))
		{
			while( isdigit(*i))
			{
				*p = *i;
				p++;
				i++;
			}


			/*SPACE CODE*/
			if(insertspace)
			{
				*p = ' ';
				p++;
			}
			/*END SPACE CODE*/
		} else if ( isalpha(*i) ) 
		{
			if(debugc) printf("Invalid Input '%s'. Only Integer input considered\n",i);
			exit(0);
		}

		if( *i == '(' )
		{
			push(&X,*i);
			i++;
		}

		if( *i == ')')
		{
			n1 = pop(&X);
			while( n1 != '(' )
			{
				*p = n1;
				p++;
				/*SPACE CODE*/
				if(insertspace)
				{
					*p = ' ';
					p++;
				}
				/*END SPACE CODE*/
				n1 = pop(&X);
			}
			i++;
		}

		if( isoperator(*i) )
		{
			if(isempty(&X))
				push(&X,*i);
			else
			{
				n1 = pop(&X);
				while(priority(n1) >= priority(*i))
				{
					*p = n1;
					p++;
					/*SPACE CODE*/
					if(insertspace)
					{
						*p = ' ';
						p++;
					}
					/*END SPACE CODE*/
					n1 = pop(&X);
				}
				push(&X,n1);
				push(&X,*i);
			}
			i++;
		}
	}
	while(!isempty(&X))
	{
		n1 = pop(&X);
		*p = n1;
		p++;
		/*SPACE CODE*/
		if(insertspace)
		{
			*p = ' ';
			p++;
		}
		/*END SPACE CODE*/
	}
	*p = '\0';
}

void emptystack_new(struct stack* s)
{
	s->top = EMPTY;
}

void push_new(struct stack* s,int item)
{
	if(debugc) printf("item is : %d\n",item);
	if(s->top == (MAX-1))
	{
		printf("\nSTACK FULL");
	}
	else
	{
		++s->top;
		s->data[s->top]=item;
	}
}

int pop_new(struct stack* s)
{
	int ret=EMPTY;

	if(s->top == EMPTY) {
		/*printf("\nSTACK EMPTY");*/
	} else
	{
	/*			printf("\nnot empty");*/
		ret= s->data[s->top];
		--s->top;
	}
	if(debugc) printf("ret is : %d\n",ret);

    	return ret;
	
}

void display_new(struct stack s)
{
	while(s.top != EMPTY)
	{
		if(debugc) printf("\n%d",s.data[s.top]);
		s.top--;
	}
}

int evaluate(char *postfix)
{
	char *p_new;
	struct stack stk;
	int op1,op2,result;
	/*char *test[10];

	


    *test = strtok(postfix, " "); 
	p_new = *test;*/
	emptystack_new(&stk);
        p_new = &postfix[0];

	while(*p_new && *p_new != '\0')
	{
	   /* removes tabs and spaces */
	   if(debugc) printf("p new : %s \n",p_new);

		while(*p_new == ' ' || *p_new == '\t') {

		
			p_new++;
		}
	  /* if is digit */
		if(isdigit(*p_new))
		{

			push_new(&stk,*p_new-48);
			if(debugc) printf("Test %d",stk.data[stk.top]);
		}
		else
		{
		   /* it is an operator */
		if(debugc) printf("before pop 1\n");
			op1 = pop_new(&stk);
					   
			op2 = pop_new(&stk);


		if(debugc) printf("p new is : %s\n",p_new);
/* To check the end of space. Maybe we need better error handling here */
      if(op2 != -1) {
			switch(*p_new)
			{
				case '+':
					result = op2 + op1;
					break;

				case '-':
					result = op2 - op1;
					break;

				case '/':
					result = op2 / op1;
					break;

				case '*':
					result = op2 * op1;
					break;

				case '%':
					result = op2 % op1;
					break;

				default:
					if(debugc) printf("\nInvalid Operator");
					return 0;
			}
	  }
			push_new(&stk,result);
		}
		p_new++;
	}
/*	printf("new result is : %d\n",result);*/
	result = pop_new(&stk);
	return result;
}


char* trimmed(char *str)
{
	char *buff;
	int start=0,end=0, i=0, j=0;
	end = strlen(str);
	
	if(debugc==1) printf("start=%d,end=%d",start,end);	
	while(str[start] == ' ')  start++;
	while (str[end-1] == ' ')  end--;	
	if(debugc==1) printf("start=%d,end=%d",start,end);
	
	buff = (char *)malloc((end-start+1)*sizeof(char));	
	if (buff==NULL) 
	{
		printf("Memory allocation problem: Please try again\n");
		exit(1);
	}
	j=0;
	for (i=start; i<end; i++) 
		buff[j++] = str[i];
	buff[j] = '\0';
	return buff;
	
}

int evaluate_cmd(char *postfix)
{
	char **args = (char**)calloc(256, sizeof(char*)); 
	char *temp_str, *str;
	char op;
	int i=0;
	int first, second, total, result;
	int fwd_pos=0, stored_pos=0;
	int stored_val[50];
	
	str = trimmed(postfix);
	
	temp_str = strtok(str, " ");
	args[i++] = temp_str;
	while (1) 
	{
		temp_str = strtok(NULL, " ");
		if(temp_str == NULL)
			break;
		args[i++] = temp_str;
	}
	
	total = i;
	if(debugc) printf(",%c,\n", args[total-1][0]);
	if(args[total-1][0] == ' ') 
		total--;
	
	while(1)
	{
		while (isoperator(args[fwd_pos][0]) == 0)
		{
			stored_val[stored_pos++] = atoi(args[fwd_pos]);
			fwd_pos++;
			continue;
		}
		first = stored_val[stored_pos-2];
		second = stored_val[stored_pos-1];
		
		if(debugc) printf("f=%d, s=%d, o=%c,\n", first, second, args[fwd_pos][0]);
		
		switch(args[fwd_pos][0])
		{
			case '+':
				result = first + second;
				break;

			case '-':
				result = first - second;
				break;

			case '/':
				result = first / second;
				break;

			case '*':
				result = first * second;
				break;

			case '%':
				result = first % second;
				break;

			default:
				if(debugc) printf("\nInvalid Operator");
				return 0;
		}
		
		stored_val[stored_pos-2] = result;
		stored_pos--;
		fwd_pos++;
		
		if(debugc) printf("r=%d, fwd_pos=%d, total = %d\n", result, fwd_pos, total);
		
		if(fwd_pos >= total)
			break;
		if(isdigit(args[fwd_pos][0]) ==0 && isoperator(args[fwd_pos][0]) == 0)
			break;
		
	}
	
	return stored_val[0];
	
}

int evaluate_exp(char *in)
{
	char post[50];
	int res;
        char exp[MAX];

	strcpy(&post[0],"");
	/*printf("Enter Expression ( eg .. 2+3*4+1 ): ");
	fflush(stdin);
	gets(in);
	
	printf("a1=%s, a2=%s\n, l=%d", argv[1], argv[2],strlen(argv[1]));
	for(i=0; i<strlen(argv[1]); i++)
	{
		in[i] = argv[1][i];
	}
	in[i] = '\0';
	printf("a1=%s,\n", &in[0]);
	*/
	parseExpr(&in[0],&post[0],1);
	if(debugc) printf("post=%s\n", post);
	res = evaluate_cmd(&post[0]);
	if(debugc) printf("Result of %s is %d\n",&in[0],res);
	return res;
}

