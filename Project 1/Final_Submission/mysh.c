#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include <signal.h>
#include <setjmp.h>
#include <errno.h>
#include "calc.h"


#define STD_INPUT 0
#define STD_OUTPUT 1
#define TOKEN_SIZE 64

#define MAXLEN 1000

typedef struct
{
	int argc;
	char **argv;
}CommandStruct;

struct Variable {
	char name[128];
	int value;
};
struct Variable variables[MAXLEN];

char my_home[50],my_prompt[10],my_src_path[10][50],buffer[100];
int debug=0;
int SPAWN=0;
jmp_buf jump_buffer;

char* malloc_char(int nBytes);
int Initialization(void);
int Read_mysh_profile(void);
int executeNormal(CommandStruct* CommandLine,CommandStruct* CommandLine_F);
int processCommand(CommandStruct* CommandLine);
int executeCD(CommandStruct* CommandLine);
CommandStruct* parseLine(char *str);
int executePipe(CommandStruct* CommandLine,CommandStruct* CommandLine_P,CommandStruct* CommandLine_F);
int execute_cmds_pipe(CommandStruct* CommandLine,CommandStruct* CommandLine_P,CommandStruct* CommandLine_F);

int executeNested(CommandStruct* CommandLine,CommandStruct* CommandLineN,CommandStruct*  CommandLine_F);
int executeFre(CommandStruct* CommandLine,CommandStruct* CommandLine_F);
int executeInt(char* int_var, char* int_val);
int findVariable(char* buff);
char* trimmed_str(char *str);
int deletefile(char *filename);
 void ctrl_CHandler(int param);
int spawnpid(void);

int main(void)
{		
		int j,chk=0;
		CommandStruct* CommandLine = (CommandStruct*) malloc(sizeof(CommandStruct)); 
		if (CommandLine==NULL) 
		{
			printf("Memory allocation problem: Please try again\n");
			exit(1);
		}
		signal(SIGINT, ctrl_CHandler);
		setjmp(jump_buffer);
		chk=Initialization();				/* initialize home directory, prompt, src directory*/
		if(chk==0)
		  printf("Not intialized properly...Please try again\n");
		chk=Read_mysh_profile();			/* read profile file*/
		chdir(my_home);					/* go to home directory */
		
		while(1)
		{
			CommandLine->argv = (char**)calloc(256, sizeof(char*)); 
			if (CommandLine->argv==NULL) 
			{
			   printf("Memory allocation problem: Please try again\n");
			   exit(1);
			}
			fflush(stdout);
			fflush(stdin);
			printf("%s",my_prompt);		/* print "mysh>"*/
			chk=processCommand(CommandLine);
			if(chk==0) continue;
		}
		return 1;
}

/******************************************************************************
 * Function         : ctrl_CHandler
 *
 * Description      : This section of code implements the signal handler. When
 *                    When we press ctrl-c it asks the user whether he wants
 *                    to exit or not.
 *
 * Input Argument   : int param
 *
 * Output Argument  : None
 *
 * Return Value     : None
 *****************************************************************************/
 void ctrl_CHandler(int param)
{
   char alter[TOKEN_SIZE];
       
   printf("\n Exit from the shell: Are you sure? [yes/no]: ");
   scanf("%31s", alter);
       
   if((strcmp(alter, "y") == 0) || (strcmp(alter, "Y") == 0) || (strcmp(alter, "yes") == 0)
   || (strcmp(alter, "YES") == 0))
      exit(0);
   else
      signal(SIGQUIT, ctrl_CHandler);
   longjmp(jump_buffer,1);
}

int spawnpid(void)
{
		int status;
		pid_t pid;
		
		pid=fork();
		if (pid < 0)
		{
			printf("Error: Fork failed\n");
			return 0;
		}
		if (pid!=0)
		{
				printf("Process ID : %d\n",pid);
				wait(&status);
		}
		else
		{	
			exit(1);
		}
		
		return 1;

}
/* Read command line arguments, parse them and then call execute module*/

int processCommand(CommandStruct* CommandLine)
{
	int i,chk,pos_ex=0,pos_gt=0, len=0;
	int flagP=0,flagFre=0, flagNest=0, flagInt=0;
	char *pipe_str,*ex_str,*temp_str,*str,*fst_str, *nest_str,*debug_str;
	char *int_val, *int_var;
	CommandStruct *CommandLine_P, *CommandLine_F, *CommandLine_N;

		SPAWN = 0; 
    if(fgets(buffer, 100, stdin)==NULL)
	{
	  printf("Error: fgets() : unable to read \n");
	  return 0;
	}
	
	if(strcmp(buffer,"\n")==0) return 0;
	strcpy(strchr(buffer, '\n'), "\0"); 
	if(strcmp(buffer,"spawn")==0)
	{
		chk=spawnpid();
		return 1;
	}
	
	if(strcmp(buffer,"exit")==0)   /* if command == exit*/
	{
		exit(1);
	}

	
	str = (char *) malloc(strlen(buffer)*sizeof(char));
	
	if(debug==1) printf("command in: %s\n",buffer);
	/* parse command line arguments */
	strcpy(str, buffer);
	temp_str = strtok(str, ">");
	str=temp_str;
	temp_str = strtok(NULL, ">");
	debug_str = strtok(NULL, ">");
	if(temp_str!=NULL)
	{
		strcpy(buffer, str);
		flagFre=1;
		ex_str=trimmed_str(temp_str);
		CommandLine_F = parseLine(ex_str);
		if((CommandLine_F->argc > 1)||(CommandLine_F->argc <1))
		{
		    printf("Invalid command\n");
			return 0;
		}
		if (debug_str!=NULL)
		{
			printf("Error: multiple '>' is not supported\n");
			return 0;
		}
		/*printf(" > = %s, %s, %s\n",ex_str, CommandLine_F->argv[0],CommandLine_F->argv[1]);*/
	}
	else 
	{
		str = strcpy(str, buffer);
		CommandLine_F = (CommandStruct*) malloc(sizeof(CommandStruct)); 
		CommandLine_F->argc=0;
	}
	
	/*check if this is an integer command */
	temp_str = strtok(str, "=");
	int_var = trimmed_str(temp_str);
	temp_str = strtok(NULL, "=");
	if(temp_str!=NULL)
	{	/* integer variable command */
		flagInt = 1;		
		int_val=trimmed_str(temp_str);
		/*check more than 3 args: "a=0"*/
		temp_str = strtok(NULL, " =");
		if(temp_str!=NULL)
		{
			flagInt=0;
			printf("Error: Wrong Input\n");
			return 0;
		}
		if(debug==1) printf("integer:%s,%s\n",int_var,int_val);
	}
	else 
	{	/*check if this has Pipe into it */
		str = strcpy(str, buffer);
		temp_str = strtok(str, "|");
		fst_str=temp_str;
		temp_str = strtok(NULL, "|");
		debug_str = strtok(NULL, "|");
		if(temp_str!=NULL)
		{	/* Pipe exists */
			flagP=1;
			pipe_str=temp_str;
			CommandLine_P = parseLine(pipe_str);
			if (debug_str!=NULL)
			{
				printf("Error: multiple '|' is not supported\n");
				return 0;
			}
		}
		else
		{	/*Pipe does not exist - check for nested */
			str = strcpy(str, buffer);
			temp_str = strtok(str, "$");
			fst_str = temp_str;
			temp_str = strtok(NULL, "$");
			debug_str = strtok(NULL, "$");
			if(temp_str!=NULL && temp_str[0] == '(')
			{
				/*nesting exist */
				flagNest = 1;
				nest_str=trimmed_str(temp_str);
				if(debug==1) printf("next=%s\n",nest_str);
				len=strlen(nest_str);
				if(nest_str[0] == '(' && nest_str[len-1] == ')') 
				{
					nest_str[0] = ' ';
					nest_str[len-1] = ' ';
				}
				else 
				{
					printf("Error: Incorrect Command\n");
					return 0;
				}
				CommandLine_N = parseLine(nest_str);
				if (debug_str!=NULL)
				{
					printf("Error: multiple '$' is not supported\n");
					return 0;
				}
			}
			else 
				fst_str = strcpy(str, buffer);
		}
		CommandLine = parseLine(fst_str);
		if(strcmp(CommandLine->argv[0],"exit")==0)   /* if command == exit*/
		{
			exit(1);
		}
	}		
	
	/* execute command */
	if (flagInt == 1)
	{
		executeInt(int_var, int_val);
	}
	else if (flagP==1)
	{
		if(debug==1) printf("Pipe=%s , %s\n",CommandLine->argv[0],CommandLine_P->argv[0]);
		/*executePipe(CommandLine,CommandLine_P,CommandLine_F);	*/	
		execute_cmds_pipe(CommandLine,CommandLine_P,CommandLine_F);	
	}
	else if(flagNest==1)
	{
		executeNested(CommandLine,CommandLine_N,CommandLine_F);
	}
	else if ((strcmp(CommandLine->argv[0],"cd")==0)||(strcmp(CommandLine->argv[0],"chdir")==0))
	{
		chk=executeCD(CommandLine);
	}
	else
	{
		executeNormal(CommandLine,CommandLine_F);
	}
	return 1;
}


int executeInt(char* int_var, char* int_val)
{
int i,j,k,len,next, chk;
	int value, assign=1; 
	char *temp, temp_var[256];
	char expression[50];
	char buff[50];
	
	/*check if int_var is alpha_numeric*/
	len = strlen(int_var);
	if(debug==1) printf("var=%s,value=%s,len=%d\n",int_var, int_val, len);
	
	for(i=0; i<len; i++)
	{
		if(isalpha(int_var[i]) || isdigit(int_var[i])) 
		{}
		else if(isspace(int_var[i]))
		{
			printf("Error:no space is allowed in variable name\n");
			return 0;
		}
		else
		{
			printf("Error: only alpha numeric characters allowed in variable name\n");
			return 0;
		}
	}
	/*check if int_val is numeric*/
	len = strlen(int_val);
	for(i=0; i<len; i++)
	{
		if( isdigit(int_val[i]) == 0 )
			assign = 0;
	}
	
	
	if(assign==1)
	{
		/* its an assignment */
		value = atoi(int_val);			
	} 
	else 
	{
		/* right side is an expression - evaluate it */
		j = 0;
		for(i=0; i<len; i++)
		{
			if(int_val[i] == '(' || int_val[i] == ')' || isdigit(int_val[i]) || isspace(int_val[i]) || isoperator(int_val[i]))
			{
				expression[j] = int_val[i];
				j++;
			}
			else if(int_val[i] == '$')
			{
				k = 0;
				temp_var[k++] = int_val[i++] ;
						
				while(1)
				{  /*form temp variable*/
					if(isdigit(int_val[i]) || isalpha(int_val[i]))
					{
						temp_var[k] = int_val[i] ;
						i++;
						k++;
					}
					else
					{
						i--;
						break; 
					}
				}
				temp_var[k] = '\0';
				if(strlen(temp_var) == 1) 
				{
					printf("Error: No variable name after $ sign");
					return 0;
				}
				else 
				{
					/*find the variable and if exists - replace the value*/
					if(debug) printf("tempvar = %s\n", temp_var);
					chk = findVariable(temp_var);
					if(chk <= 0)
					{
						printf("Error: No variable with name: $%s\n", temp_var);
						return 0;
					}
					else
					{
						sprintf(buff, "%d\0", variables[chk-1].value);
						k = 0;
						while (buff[k] != '\0')
							expression[j++] = buff[k++];
					}
				}
				
			}			
			else
			{
				printf("Error: Not a valid expression on right side\n");
				return 0;
			}
		}
		expression[j] = '\0';
		if (debug == 1) printf("expression : %s\n", expression);
		value = evaluate_exp(expression);
	}
	
	/*convert value into integer*/
	
	/*check if the variable name is an enviornment variable*/
	strcpy(temp_var, int_var);
	temp = getenv(temp_var);
	if(debug==1) printf("var1=%s,value=%d,temp=%s\n",int_var, value, temp);
	if(temp != NULL) {
		printf("Error: cannot take enviornment variable as generic variable name\n");
		return 0;
	}

	/*check if variable already exists*/
	for (i=0; strlen(variables[i].name) != 0; i++)
	{
		if(strcmp(variables[i].name, int_var)==0)
		{   /*variable already exists so update the value*/
			variables[i].value = value;
		}
	}
	
	/*valiable does not exists so create a new one*/
	strcpy(variables[i].name, int_var);
	variables[i].value = value;
	
	printf("%s=%d\n", variables[i].name, variables[i].value);
	
	return 1;
}

CommandStruct* parseLine(char *str)
{
	int i, chk;
	char *temp_str;
	char *buff;
	CommandStruct* CommandLine_temp = (CommandStruct*) malloc(sizeof(CommandStruct)); 
	if (CommandLine_temp==NULL) 
	{
		printf("Memory allocation problem: Please try again\n");
		exit(1);
	}
	CommandLine_temp->argv = (char**)calloc(256, sizeof(char*)); 
	if (CommandLine_temp->argv==NULL) 
	{
		printf("Memory allocation problem: Please try again\n");
		exit(1);
	}
	CommandLine_temp->argv[0] = strtok(str, " ");
	
	i=1;
	while(1)
	{
			chk = 0;
			buff = (char *) malloc(64*sizeof(char));
			temp_str=strtok(NULL," ");
			if(temp_str == NULL)
			{
				CommandLine_temp->argv[i]=temp_str;
				break;
			}
			else if(strcmp(temp_str, "&")==0)
			{
				SPAWN = 1;
				CommandLine_temp->argv[i] = NULL;
				if(debug == 1) printf("spawn=%d\n", SPAWN);
				break;
			}
			else if(temp_str[0] == '$' && (chk=findVariable(temp_str)) > 0)
			{
				if(debug==1) printf("2dollar=%s,chk=%d\n",temp_str, chk);
				sprintf(buff, "%d", variables[chk-1].value);
				CommandLine_temp->argv[i] = buff;
			}
			else 
				CommandLine_temp->argv[i]= temp_str;
			i++;
	}
	/*printf("parse > =%s %s\n",CommandLine_temp->argv[0],CommandLine_temp->argv[1]);*/
	CommandLine_temp->argc=i;
	return CommandLine_temp;
}

int findVariable(char* buff)
{
	char* temp;
	int i, len;
	len = strlen(buff);
	if(debug == 1) printf("buff = %s, len=%d\n", temp, len);
	
	temp = (char *) malloc((len -1)*sizeof(char));
	for (i=1; i<len; i++)
		temp[i-1] = buff[i];
	temp[i-1] = '\0';
	
	for(i=0; i<1000; i++)
	{
		if(strcmp(variables[i].name, temp) == 0)
			return i+1;
		if(strlen(variables[i].name) == 0)
		    break;
	}
	
	return 0;
}

int execute_cmds_pipe(CommandStruct* CommandLine,CommandStruct* CommandLine_P,CommandStruct* CommandLine_F)
{

	int i,i2,sender_pid,reciver_pid;
	int fd[2];
	int status;

	fflush(stdout);

	pipe(&fd[0]);

	if((reciver_pid =fork())!=0)
  	{
		if((sender_pid = fork())!=0)
		{
			status = 0;

			waitpid(sender_pid,&status,0);
			if(debug==4) printf("Sender status returned: %i\n",WEXITSTATUS(status));
		

			close(fd[1]);
			waitpid(reciver_pid,&status,0);
  			if(debug==4) printf("Reciever status returned: %i\n",WEXITSTATUS(status));


  			if(debug==4) printf("Done with reciver_ptr %i and sender_ptr %i\n",reciver_pid,sender_pid);
		}
		else
		{
			close(fd[0]);
			close(STD_OUTPUT);
			dup(fd[1]);
			close(fd[1]);
			execvp(CommandLine->argv[0], CommandLine->argv);
			if(debug==1) printf("Error: execv failed\n");
			printf("%s: not found\n",CommandLine->argv[0]);
				exit(1);
			/*i2 = execve(cmd1,in_argv1,envp);
			

			if(i<0)
			{
				
				exit(1);
			}
			exit(0);*/

		}

  	}
  	else
  	{
		close(fd[1]);
		close(STD_INPUT);
		dup(fd[0]);
		close(fd[0]);
		if(CommandLine_F->argc==1)
		{
		  if(freopen(CommandLine_F->argv[0],"w",stdout)==NULL)
		  {
			printf("File refirection error\n");
			return 0;
		  }
		}
		execvp(CommandLine_P->argv[0], CommandLine_P->argv);
		if(debug==1) printf("Error: execv failed\n");
		printf("%s: not found\n",CommandLine_P->argv[0]);
		exit(1);
	/*	i = execve(cmd2,in_argv2,envp);
		

		if(i<0)
		{
			
			exit(1);
		}
		exit(0);*/
	}


}



int executePipe(CommandStruct* CommandLine,CommandStruct* CommandLine_P,CommandStruct* CommandLine_F)
{
	int fd[2],fd_out,status;
	pid_t pid, pid2;
	
	if(debug==1) printf("ExecutePipe=%s , %s,%s\n",CommandLine->argv[0],CommandLine_P->argv[0],CommandLine_P->argv[1]);

	pipe(fd);
	if ((pid=fork()) < 0) 
	{
		printf("Error: Fork failed\n");
		return 0;
	}
	if(pid==0)  /* right side of | */
	{
		close(fd[1]);
		dup2(fd[0],0);
		close(fd[0]);
		if(CommandLine_F->argc==1)
		{
		  if(freopen(CommandLine_F->argv[0],"w",stdout)==NULL)
		  {
			printf("File refirection error\n");
			return 0;
		  }
		}
		execvp(CommandLine_P->argv[0], CommandLine_P->argv); 
		if(debug==1) printf("Error: execv failed\n");
		printf("%s: not found\n",CommandLine_P->argv[0]);
		exit(1);
	}
	else 
	{
		if((pid2=fork())< 0)
		{
			printf("Error: Fork failed\n");
			return 1;
		}
		if(pid2==0)   /* left side of | */
		{
			close(fd[0]);
			dup2(fd[1],1);
			close(fd[1]);
			execvp(CommandLine->argv[0], CommandLine->argv);
			if(debug==1) printf("Error: execv failed\n");
			printf("%s: not found\n",CommandLine->argv[0]);
			exit(1);
		}
		else
		{
			close(fd[0]);
			close(fd[1]);
			while (wait(&status) != pid2);
		}
	}
	
	return 1;
}

/* Execute cd , chdir commands*/
int executeCD(CommandStruct* CommandLine)
{
	char *dir;
	if(CommandLine->argc==2)
	    dir=CommandLine->argv[1];
	else if(CommandLine->argc==1)
		dir=my_home;
	else
	{
		printf("%s: too many arguments\n",CommandLine->argv[0]);
	}
	
	if(dir!=NULL)
	{
		if(debug==1) printf("Changing directory to: %s\n",dir);
		if(chdir(dir)<0)
		{
			switch(errno)
			{
				case ENOENT:
					fprintf(stderr,"directory not found\n");
					break;
				case ENOTDIR:
					fprintf(stderr,"not a directory name\n");
					break;
				case EACCES:
					fprintf(stderr,"you don't have right to access\n");
					break;
				default:
				fprintf(stderr,"Error: please try again\n");
			}
		}
	}


	return 1;
}

/* Execute commands with or without argument such as: cat, ls*/

int executeNormal(CommandStruct* CommandLine,CommandStruct* CommandLine_F)
{
		
		int status;
		pid_t pid;
		
		pid=fork();
		if(debug==1) printf("pid: %d, argv=%s, fc=%d\n",pid, CommandLine->argv[0], CommandLine_F->argc);
		
		if (pid < 0)
		{
			printf("Error: Fork failed\n");
			return 0;
		}
		if (pid!=0)
		{
			if(SPAWN==0)
			{
				/*wait if not a spawn process */
				wait(&status);
			}
			else 
			{
				printf("spawned a new process with id: %d\n", pid);
			}

		}
		else
		{	
			if(CommandLine_F->argc==1)
				if(freopen(CommandLine_F->argv[0],"w",stdout)==NULL)
				{
					printf("File refirection error\n");
					return 0;
				}
			execvp(CommandLine->argv[0],CommandLine->argv);
			if(debug==1) printf("Error: execv failed\n");
			printf("%s: not found\n",CommandLine->argv[0]);
			exit(1);
		}
		
		return 1;
}

/* Read_mysh_profile(); */
int Read_mysh_profile(void)
{
		char file_name[15],line_string[100];
		char word[10];
		FILE* file_handler;
		strcpy(file_name,".mysh_profile");	
		
		file_handler = fopen (file_name, "r");
				
		if (file_handler != NULL)
		{
			if(debug==1) printf("file opened\n");
			while(fgets(line_string,100,file_handler)!=NULL)
			{
				if(debug==1) printf("\nline_string=%s",line_string);
				strcpy(word,strtok(line_string," ;"));
				if(debug==1) printf("wordlist=%s\n",word);
				if(strcmp(word,"HOME")==0)
				{
					strcpy(my_home,strtok(NULL,"\n"));
				}
				else if(strcmp(word,"PROMPT")==0)
				{
					strcpy(my_prompt,strtok(NULL,"\n"));
				}
				else if(strcmp(word,"PATH")==0)
				{
					strcpy(my_src_path[0],strtok(NULL,";"));
					strcpy(my_src_path[1],strtok(NULL,"\n"));
				}

			} /* while (fgets!=NULL) */
				fclose(file_handler);
		} 
		else /* if(file_handler == NULL) */
		{
			perror (file_name);
			return 0;
		}
		if(debug==1)
		{
			printf("HOME = %s\n",my_home);
			printf("PROMPT = %s\n",my_prompt);
			printf("PATH 1= %s\n",my_src_path[0]);
			printf("PATH 2= %s\n",my_src_path[1]);
		}
		
		return 1;
}

int executeNested(CommandStruct* CommandLine, CommandStruct*  CommandLineN, CommandStruct*  CommandLine_F)
{
	char *arg1[] = {"fgrep", "-l", "malloc", "x.c", (char *) 0};
	char *arg2[] = {"wc", "temp2.txt", (char *) 0};
	char **N_argv;
	char *filename;
	char buff[2048];
	char tempbuff[128];
	char tempstr[128];
	char *str1;
	pid_t pid;
	int status,i,len,j,flag=0,cnt=0,k;
	int fLength = 0;
	FILE *fd1;
	FILE *fd2; 
  
	filename = tmpnam(NULL);    /*create temporary file*/
	if(debug==1) printf("executeNested %s, %s\n",CommandLineN->argv[0],CommandLine->argv[0]);
	N_argv = (char**)calloc(256, sizeof(char*));
	if (N_argv==NULL) 
	{
		printf("Memory allocation problem: Please try again\n");
		return 0;
	}
	
	for(k=0;k<256;k++)
	{
		N_argv[k] = (char*)malloc(256*sizeof(char)); 
		if (N_argv[k]==NULL) 
		{
			printf("Memory allocation problem: Please try again\n");
			return 0;
		}
	}
	
	/* executing first command and storing output in temporary file*/
	
	pid = fork();
	if (pid < 0)
	{
		printf("Error: Fork failed\n");
		return 0;
	}
	if(pid != 0)
	{
		wait(&status);
	} 
	else 
	{
		if(freopen(filename,"w",stdout)==NULL)
		{
			printf("File refirection error\n");
			return 0;
		}
		execvp(CommandLineN->argv[0], CommandLineN->argv);
		if(debug==1) printf("Error: execv failed\n");
		printf("%s: not found\n",CommandLineN->argv[0]);
		exit(1);
	}   
	
	fd2 = fopen(filename, "r");
	if (fd2 == NULL)
	{
	   printf("Temporary file read error\n");	
	   return 0;  
	}
	fseek(fd2, 0, SEEK_END);
	fLength = ftell(fd2);
    if ((str1 = malloc(fLength * sizeof(char) + 1)) == NULL)
	{
       printf("Memory allocation problem: Please try again\n");
	   fclose(fd2); 
	   return 0;   
	}
	fseek(fd2, SEEK_SET, 0);
	fread(str1, sizeof(char), fLength, fd2);
	str1[fLength + 1] = '\0';
	if(debug==1) printf("str1:%s",str1);
	fclose(fd2);
   
	for(i=0; i< CommandLine->argc; i++)
	{
		N_argv[i]=CommandLine->argv[i];
		if(debug==1) printf("1:N_argv[%d]=%s\n",i,N_argv[i]);
		cnt++;
	}
	flag=0;
   
	N_argv[i] = strtok(str1, " \n");
	while(N_argv[i] != NULL)
	{
		i++;
		N_argv[i] = strtok(NULL, " \n");
	}
   
	for(j=0; N_argv[j]!=NULL && debug == 1;j++)
	{
		printf("3:N_argv[%d]=%s\n",j,N_argv[j]);
	}
	
	pid = fork();
	if (pid < 0)
	{
		printf("Error: Fork failed\n");
		return 0;
	}
	if(pid != 0)
	{
		wait(&status);
	}	
	else 
	{
		if(CommandLine_F->argc==1)
			if(freopen(CommandLine_F->argv[0],"w",stdout)==NULL)
			{
				printf("File refirection error\n");
				return 0;
			}
		execvp(CommandLine->argv[0],N_argv);
		if(debug==1) printf("Error: execv failed\n");
		printf("%s: not found\n",CommandLine->argv[0]);
		exit(1);
	} 
	deletefile(filename);
	return 1;
}

int Initialization(void)
{
	strcpy(my_home,"/home");
	strcpy(my_prompt,"mysh>");
	strcpy(my_src_path[0],"/usr/bin");
	strcpy(my_src_path[1],"/bin");
	return 1;
}

char* malloc_char(int nBytes)
{	char *temp;
	temp = (char *) malloc(sizeof(char)*nBytes);
	if (temp==NULL) 
	{
		printf("Memory allocation problem: Please try again\n");
		exit(1);
	}
	return temp;
}

int deletefile(char *filename)
{
	pid_t pid;
	int status;
	pid = fork();
	if (pid < 0)
	{
		printf("Error: Fork failed\n");
		return 0;
	}
    if(pid != 0)
    {
		wait(&status);
    } 
	else 
    {
		execlp("rm", "rm", "-f", filename, (char *) 0);
		printf("Error: execlp failed\n");
		exit(1);
	} 
	
}

char* trimmed_str(char *str)
{
	char *buff;
	int start=0,end=0, i=0, j=0;
	end = strlen(str);
	
	if(debug==1) printf("start=%d,end=%d",start,end);	
	while(str[start] == ' ')  start++;
	while (str[end-1] == ' ')  end--;	
	if(debug==1) printf("start=%d,end=%d",start,end);
	
	buff = (char *)malloc((end-start+1)*sizeof(char));	
	if (buff==NULL) 
	{
		printf("Memory allocation problem: Please try again\n");
		exit(1);
	}
	j=0;
	for (i=start; i<end; i++) 
		buff[j++] = str[i];
	buff[j] = '\0';
	return buff;
	
}
