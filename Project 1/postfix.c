#include <stdio.h>
#include <ctype.h>

#define MAX 50
#define EMPTY -1

struct stack
{
	int data[MAX];
	int top;
};

void emptystack(struct stack* s)
{
	s->top = EMPTY;
}

void push(struct stack* s,int item)
{
	if(s->top == (MAX-1))
	{
		printf("\nSTACK FULL");
	}
	else
	{
		++s->top;
		s->data[s->top]=item;
	}
}

int pop(struct stack* s)
{
	int ret=EMPTY;
	if(s->top == EMPTY)
		printf("\nSTACK EMPTY");
	else
	{
		ret= s->data[s->top];
		--s->top;
	}
	return ret;
}

void display(struct stack s)
{
	while(s.top != EMPTY)
	{
		printf("\n%d",s.data[s.top]);
		s.top--;
	}
}

int evaluate(char *postfix)
{
	char *p;
	struct stack stk;
	int op1,op2,result;

	emptystack(&stk);
	p = &postfix[0];

	while(*p != '\0')
	{
	   /* removes tabs and spaces */
		while(*p == ' ' || *p == '\t')
		{
			p++;
		}
	  /* if is digit */
		if(isdigit(*p))
		{
			push(&stk,*p - 48);
		}
		else
		{
		   /* it is an operator */
			op1 = pop(&stk);
			op2 = pop(&stk);

			switch(*p)
			{
				case '+':
					result = op2 + op1;
					break;

				case '-':
					result = op2 - op1;
					break;

				case '/':
					result = op2 / op1;
					break;

				case '*':
					result = op2 * op1;
					break;

				case '%':
					result = op2 % op1;
					break;

				default:
					printf("\nInvalid Operator");
					return 0;
			}
			push(&stk,result);
		}
		p++;
	}
	result = pop(&stk);
	return result;
}

int main()
{
	char exp[MAX];
	printf("Enter Postfix Expression : ");
	gets(exp);
	printf("%s EQUALS %d\n",exp,evaluate(&exp[0]));
	return 0;
}
/* SAMPLE OUTPUT:
Enter Postfix Exp<b></b>ression : 3 5 + 2 /
3 5 + 2 / EQUALS 4
*/
