/* 
CS551 PROJECT-1 DVELOPE YOUR OWN SHELL
TEAMS MEMBERS:
                   RADHIKA RAMAMORTHY - A20243303
									 AARTHY ASHOK       - A20242389
									 SHYAM GAJJAR       - A20244964
*/


/*****************************************************************************
 * FILENAME         : Create_Shell.c
 *
 * DESCRIPTION      : Source file to create shell and run your own shell in
 *                    terminal.
 *
 * HISTORY          : 
 *
 * DATE            NAME           DRAFT
 * FEB/01/2010,    Shyam,     Created the file.
 * FEB/03/2010,    Aarthy,    Implement a function.
 * FEB/09/2010,    Radhika,   Implement a function.
 *****************************************************************************/

/******************************************************************************
 *                               Includes                                     *
******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <setjmp.h> 
#include <minix/minlib.h>
#include <curses.h>
#include <fcntl.h>

/******************************************************************************
 *                                Defines                                     *
******************************************************************************/
#define FALSE						           0
#define TRUE						           1
#define COMMAND_LENGTH  			     512
#define TOKEN_SIZE			           64
#define MAXIMUM_TOKENS			       COMMAND_LENGTH/TOKEN_SIZE
#define STANDARD_PIPE       -1

/******************************************************************************
 *                               Structures                                   *
******************************************************************************/
typedef struct 
{
	char	prflPath[COMMAND_LENGTH];
	char	prompt[TOKEN_SIZE];
	char	bin_path[COMMAND_LENGTH];	
  char	home[COMMAND_LENGTH];

} profile;

typedef struct history
{
   char *pLine;
   char *pCommand;
   char *pAlias;
   char *pAliasCmd;
   int  iID;
   struct history *pNextAddress;
}history;

typedef struct command_pipe 
{
   int write_pipe;
   int read_pipe;
} command_pipe;

/******************************************************************************
 *                                Global Variables                            *
******************************************************************************/
int status;
int redirection_type;
jmp_buf jump_buffer;
history *pStart = NULL, *pEnd = NULL;

/******************************************************************************
 *                             Function Declarations                          *
******************************************************************************/

/******************************************************************************
 * Function         : getTokens
 *
 * Description      : This function contains the code that Creates the token
 *                    based on space, newline and tab.
 *
 * Input Argument   : char *name
 *                    char **retu_token
 *
 * Output Argument  : None
 *
 * Return Value     : None
 *****************************************************************************/
void getTokens (char* name, char** retu_token)
{
   int inc = 0;
	 char* inst ;

	 for(inst = strtok(name," \n\t"); inst != NULL; inst = strtok(NULL," \n\t")) 
	 {
		  if (strcmp(inst, " ") != 0)
		     retu_token[inc++] = inst;
   } 
   retu_token[inc] = NULL; 
}

/******************************************************************************
 * Function         : RunFnct
 *
 * Description      : This section of code Create tokens from the command and
 *                    removes '&' from the token.
 *
 * Input Argument   : char **argmt
 *
 * Output Argument  : None
 *
 * Return Value     : None
 *****************************************************************************/
int RunFnct(char**  argmt)
{
   int id, status;
   if (( id =fork()) < 0)
   {
      printf(" \n FORK Process not executed properly\n");
      exit(1);
   }
   else if (id==0)
   {
      execvp(*argmt, argmt);
   }
   else 
   {  
      while(wait(&status)!=id);
   }
   return TRUE;
}

/******************************************************************************
 * Function         : RemoveTokens
 *
 * Description      : 
 *
 * Input Argument   : char *name
 *                    char **com_token
 *
 * Output Argument  : None
 *
 * Return Value     : None
 *****************************************************************************/
void RemoveTokens(char* name, char** com_token)
{
   int inc = 0;
   char* inst;
   char* str = "&";
   for(inst = strtok(name," =\n\t");inst != NULL;inst = strtok(NULL," =\n\t"))
   {
      if(strcmp(inst, " ") != 0)
      com_token[inc] = inst;
   }
   inc = inc-1;
   if(com_token[inc] == inst)
      com_token[inc] = NULL;
   else
   { 
      inc=inc+1; 
      com_token[inc] = NULL;
   } 
}

/******************************************************************************
 * Function         : GetPipedCommands
 *
 * Description      : This section of code Searches for pipe in the command
 *                    and separates the command and tokenizes on bases of '|'.
 *
 * Input Argument   : char *name
 *                    char **com_token
 *
 * Output Argument  : None
 *
 * Return Value     : None
 *****************************************************************************/
int GetPipedCommands (char* name, char** com_token)
{
   int inc = 0;
   char* inst;
   for(inst = strtok(name, "|");inst != NULL;inst = strtok(NULL,"|"))
   {           
      com_token[inc++] = inst;
   }
       
   com_token[inc] = NULL;
   return (inc-1);
}

/******************************************************************************
 * Function         : BufferClear
 *
 * Description      : This section of code clears the buffer everytime when the
 *                    command from the shell is executed.
 *
 * Input Argument   : char *buffr
 *                    int  num
 *
 * Output Argument  : None
 *
 * Return Value     : None
 *****************************************************************************/
void BufferClear(char* cBuffer, int iNum)
{
  int j;

  for (j=0;j<iNum ; j++)
    cBuffer[j] = '\0';
}

/******************************************************************************
 * Function         : gotoHome
 *
 * Description      : This section of code sets the home directory that is
 *                    defined in the .profile file
 *
 * Input Argument   : profile *user_profile
 *
 * Output Argument  : None
 *
 * Return Value     : None
 *****************************************************************************/
int gotoHome(profile* pUserProfile)
{
   int opt;
       
   opt = chdir(pUserProfile->home);
   if (opt != 0)
   {        
      printf("\n THE HOME DIRECTORY IS NOT PROPERLY SET\n");
      return FALSE;
   }
   return TRUE;
}

/******************************************************************************
 * Function         : ConfigurePrfl
 *
 * Description      : This section of code reads '.profile' file and intializes
 *                    the environment variables.
 *
 * Input Argument   : profile **ppUserProfile
 *
 * Output Argument  : None
 *
 * Return Value     : None
 *****************************************************************************/
int ConfigurePrfl(profile** ppUserProfile)
{
	FILE*	  fp_file = NULL;
  char	  profile_setting[COMMAND_LENGTH];
	char*   profile_file;
	char*   setting[MAXIMUM_TOKENS];

	profile_file = (char*)malloc(sizeof(char)*COMMAND_LENGTH);
   strcpy(profile_file, ".profile");


	*ppUserProfile = (profile*)malloc(sizeof(profile));

	strcpy((*ppUserProfile)->prflPath, profile_file);

	if ((fp_file= fopen(profile_file, "r")) != NULL)
	{
		while (!feof(fp_file))
		{
			fgets(profile_setting, COMMAND_LENGTH, fp_file);
			getTokens(profile_setting, setting);	
      if (setting != NULL && *setting != NULL && strcmp(*setting, "PROMPT") == 0)
				strcpy((*ppUserProfile)->prompt, *(setting+1));
			else if (setting != NULL && *setting != NULL && strcmp(*setting, "HOME") == 0)
				strcpy((*ppUserProfile)->home, *(setting+1));
			else if (setting != NULL && *setting != NULL && strcmp(*setting, "PATH") == 0)
				strcpy((*ppUserProfile)->bin_path, *(setting+1));		}

		fclose(fp_file);
	}
	return TRUE;
}

/******************************************************************************
 * Function         : closePipes
 *
 * Description      : This section of code closes all the pipes that were
 *                    opened during the execution of multiple pipe commands.
 *
 * Input Argument   : command_pipe *name
 *                    int          inc_final
 *
 * Output Argument  : None
 *
 * Return Value     : None
 *****************************************************************************/
void  closePipes(command_pipe* name, int inc_final)
{
   int j =0;

   for (j = 0; j<= inc_final; j++)
   {
      close(name[j].read_pipe);
      close(name[j].write_pipe);
   }
}

/******************************************************************************
 * Function         : exePipeCommand
 *
 * Description      : This section of code reads the command from the console
 *                    and if it contains pipe then it counts the number of pipes
 *                    and creates an array of size of the number of pipes. It
 *                    also creates file descriptor of every element of array
 *                    and sets the read and write descriptor. It executes the
 *                    command while the parent waits for the child to return.
 *
 * Input Argument   : char *pipe_command
 *                    int  is_parallel_process
 *
 * Output Argument  : None
 *
 * Return Value     : None
 *****************************************************************************/
int exePipeCommand(char* pipe_command, int is_parallel_process)
{
   char *pipe_tokens[MAXIMUM_TOKENS];
   char *com_token[MAXIMUM_TOKENS];
   char *tokens[MAXIMUM_TOKENS];
   char *retu_token[MAXIMUM_TOKENS];
   int pipe_count = 0, i = 0, j = 0, status;
   command_pipe* cmdPipes;
   int pipeFD[2];
   int pid, pid_array[MAXIMUM_TOKENS];
       
	 getTokens(pipe_command,retu_token);
	      
   pipe_count = GetPipedCommands(pipe_command, pipe_tokens);
      
   cmdPipes  = (command_pipe*)malloc((pipe_count+1) * sizeof(command_pipe));
             
   for (i=0; i< pipe_count; i++)
   {
	    if (pipe(pipeFD) < 0)
	    {
	       printf("Cannot create pipe\n");
	       exit(1);
	    }
	    else
	    {
	       cmdPipes[i].write_pipe = pipeFD[1];
	       cmdPipes[i+1].read_pipe= pipeFD[0];
	    } 
   }
       
   cmdPipes[0].read_pipe = STANDARD_PIPE;
   cmdPipes[pipe_count].write_pipe = STANDARD_PIPE;

   for (i=0; i<pipe_count+1; i++)
   {
      if ((pid = fork()) <0)
	    {
         printf ("Fork error\n");
	       exit(1);
	    }
	    else if (pid == 0)
	    {
         if (i != 0) dup2(cmdPipes[i].read_pipe, 0);
	   
         if (i != pipe_count) dup2(cmdPipes[i].write_pipe, 1);
     
	       retu_token[2]=NULL;
	       retu_token[3]=NULL;
	       closePipes(cmdPipes, pipe_count);
	       RemoveTokens(pipe_tokens[i], tokens);
	        
	       execvp(*tokens, tokens);
	        
	       exit (1); 
	    }
	    else
	    {
	    	pid_array[i] = pid;      
	    }
	 }
	
   for (i=0; i<pipe_count+1; i++)
	    if (is_parallel_process != TRUE)
	    {
	       waitpid(pid_array[i], &status, 0);
	       close (cmdPipes[i].read_pipe);
         close (cmdPipes[i].write_pipe);
	    }

      free(cmdPipes);
      return TRUE;
}

/******************************************************************************
 * Function         : executeCommand
 *
 * Description      : This section of code executes the command with no pipe
 *                    from the prompt. It also checks for the '&' operator and
 *                    if the operator is present then the prompt is returned
 *                    immediately after the child is executed.
 *
 * Input Argument   : char **name
 *                    int  pipe_array
 *
 * Output Argument  : None
 *
 * Return Value     : None
 *****************************************************************************/
int executeCommand(char** name,int pipe_array)
{
   int iRet = 0;
	 int pid, status;

	 if ((pid = fork()) < 0)
	 {
	   perror ("\n Not able to create a fork process\n");
	   exit(1);
	 }

   else if (pid == 0)
   {
      iRet = execvp(*name, name);
   }          
   else if(pipe_array != TRUE)
   {
      /*printf("executeCommand: In pipe\n");*/
      while (wait(&status) != pid);
	 }
   
   return TRUE;
}

/******************************************************************************
 * Function         : ctrl_CHandler
 *
 * Description      : This section of code implements the signal handler. When
 *                    When we press ctrl-c it asks the user whether he wants
 *                    to exit or not.
 *
 * Input Argument   : int param
 *
 * Output Argument  : None
 *
 * Return Value     : None
 *****************************************************************************/
 void ctrl_CHandler(int param)
{
   char alter[TOKEN_SIZE];
       
   printf("\n Exit from the shell: Are you sure? [yes/no]: ");
   scanf("%31s", alter);
       
   if((strcmp(alter, "y") == 0) || (strcmp(alter, "Y") == 0) || (strcmp(alter, "yes") == 0)
   || (strcmp(alter, "YES") == 0))
      exit(0);
   else
      signal(SIGINT, ctrl_CHandler);
   longjmp(jump_buffer,1);
}

/******************************************************************************
 * Function         : concat_commnd
 *
 * Description      : This section of code adds 2 string entered on the prompt
 *                    by the user and also performs subtraction between 2
 *                    strings.
 *
 * Input Argument   : char **param
 *
 * Output Argument  : None
 *
 * Return Value     : None
 *****************************************************************************/
int concat_commnd(char **param)
{
   char new_command[COMMAND_LENGTH];
   char *det[MAXIMUM_TOKENS];
   int p=0,q=0,r=0,l=0,m=0,t=0,x=0,y=0,z=0,n=0;
   char a[30], b[30],c[30],d[30];
   printf("**Add or Subtract according to your need**");
   if (fgets(new_command, COMMAND_LENGTH, stdin) != NULL)
   {
      getTokens(new_command, det);
      if(*det != NULL)
      {
         if((strcmp(det[0],"addition")==0) && (det[1]!=NULL))
         {			
            p=2;
            while(param[p]!=NULL)
		        {
		           if(strcmp(param[p+1],"=")==0)
               {
                  p=p+2;
               }
               else
               {                     					
                  printf("%s ",param[p]);
		              p++;
               }
		        }
    		 }
    		 else if ((strchr(det[0],'(') != NULL) && (strchr(det[2], '-') != NULL))
         {   
            printf("\n Substraction \n");
         	  strcpy(a, param[2]);
         	  strcpy(b, param[5]);
            while (p < strlen(a))
         	  {
         		   while (r < strlen(b))
         		   {
         			    if (a[p] == b[r])
         			    {
         					   c[t] = a[p];
         					   t = t + 1;
         				  }
         				  r = r + 1;
         			 }
         			 r = 0;
               p = p + 1;
            }
            while (l < strlen(c))
            {
               while (m < strlen(a))
							 {
							    if (a[m] == c[l])
									a[m] = '$';
							 	  m = m + 1;	
							 }
							 m = 0;
         			 l= l + 1;
         	  }
         	  for (p = 0; p < strlen(a); p = p + 1)
         		   if (a[p] == '$')	
         			    a[p] = ' ';
         		   printf("%s", a);
         }
         else
		   	 {
            perror("\n impropoer syntax\n");
         }
    	}
	    printf("\n");
   }      	
}

/******************************************************************************
 * Function         : run_redirected
 *
 * Description      : This section of code adds 2 string entered on the prompt
 *                    by the user and also performs subtraction between 2
 *                    strings.
 *
 * Input Argument   : char *name
 *                    char **arg_tokens
 *                    int  file_id
 *
 * Output Argument  : None
 *
 * Return Value     : None
 *****************************************************************************/
int run_redirected(char* name, char **arg_tokens, int file_id)
{  
   int inc = 0,pid;
   char* param;
   char *temp[MAXIMUM_TOKENS];

   printf("arg0:%s\n",arg_tokens[0]);
   printf("arg1:%s\n",arg_tokens[1]);
   printf("arg2:%s\n",arg_tokens[2]);
   printf("*arg_tokens:%s\n",arg_tokens);
   temp[0]= arg_tokens[0];
   temp[1]= arg_tokens[1];

   /*arg_tokens[inc] = NULL;*/
//   arg_tokens[2]=NULL;
      
   if((pid=fork()) <0)
   {
      printf("Error\n");
      return 0;
   }
   if(pid==0)
   {
      int fid;
      if(file_id==0)  
      {
         printf("\n STDIN\n");
         fid = open(arg_tokens[3], O_RDONLY,0600);
      }
      else if (file_id==1)
      {   
         printf("\n out\n");
         fid = open(arg_tokens[3], O_CREAT | O_TRUNC | O_WRONLY,0777);
      }
      
      arg_tokens[3]=NULL;
      dup2(fid,file_id); 
//      close(file_id);
       			 
      printf("%s\n",arg_tokens[3]);
       	      
      execvp(*arg_tokens,arg_tokens);
            
      printf("Command Successful\n"); 
      return 0;
   }
   else
   {
      while(wait(&status)!=pid);
   }
} 

/******************************************************************************
 * Function         : spawn_process
 *
 * Description      : This section of the code spawns a new process and
 *                    returns the process id.
 *
 * Input Argument   : int y
 *
 * Output Argument  : None
 *
 * Return Value     : None
 *****************************************************************************/
void spawn_process(int y)
{
   int pid;
	 if((pid=getpid())<0)
		  perror("Not able to return the process id");
	 else
	    printf("\nThe program's process Id is : %d\n",pid);  
}

/******************************************************************************
 * Function         : get_history
 *
 * Description      : This section of the code spawns a new process and
 *                    returns the process id.
 *
 * Input Argument   : char *name
 *                    int  strLen
 *                    int  iID
 *
 * Output Argument  : None
 *
 * Return Value     : None
 *****************************************************************************/
void get_history(char *name, int strLen, int iID)
{
   char    *tokens[MAXIMUM_TOKENS];
   history *pCurrent = NULL, *pTempCurrent = NULL;
   int     iLen = 0;

   pCurrent = (history *)malloc(sizeof(history));
   pCurrent->pNextAddress = NULL;

   pCurrent->pLine = (char *)malloc (strLen * sizeof(char));
   strcpy ((pCurrent->pLine), name);

   getTokens(name, tokens);

   iLen = strlen (tokens[0]);
   pCurrent->pCommand = (char *)malloc(iLen*sizeof(char));
   strcpy ((pCurrent->pCommand), tokens[0]);

   if ((strcmp(tokens[0],"Alias") == 0) || (strcmp(tokens[0],"alias")==0))
   {
      iLen = strlen (tokens[1]);

      pCurrent->pAlias = (char *)malloc(iLen*sizeof(char));
      strcpy((pCurrent->pAlias),tokens[1]);

      iLen = strlen (tokens[3]);
      pCurrent->pAliasCmd = (char *)malloc(iLen*sizeof(char));
      strcpy((pCurrent->pAliasCmd),tokens[3]);
   }
   else
   {
      pCurrent->pAlias = NULL;
      pCurrent->pAliasCmd = NULL;
   }
   
   pCurrent->iID = iID;

   if ((NULL == pStart) && (NULL == pEnd))
   {
      pEnd = pStart = pCurrent;
   }
   else
   {
      pEnd->pNextAddress = pCurrent;
      pEnd = pCurrent;
   }

   pTempCurrent = pStart;
   while (NULL != pTempCurrent)
   {
      pTempCurrent = pTempCurrent->pNextAddress;      
   }
}

/*
void search_history(char *name)
{
   char *pTempName = NULL;
   int  iLen = 0;

   iLen = strlen(name);
   pTempName = (char *)malloc(iLen*sizeof(char));

   strcpy(pTempName, name);

   if(pTempName[0]=='!')
   {
      if(pTempName[1]=='!')
      {
         execute_history(pTempName);
      }
     
   }

}


void execute_history(char *pTempName)
{
   history *pTemp = NULL;
   int iNumber = 0;

   for (pTemp = pStart; pTemp != NULL; pTemp = pTemp->pNextAddress)
   {
      iNumber++;
   }
   printf ("iNumber:%d\n",iNumber);

      if ('!' == pTempName[1])
      {
         for(pTemp = pStart; pTemp!=NULL; pTemp=pTemp->pNextAddress)
	 {
	    if ((iNumber-1) == (pTemp->iID))
	    {
	       
	    }
	 }
      }
}
*/

/******************************************************************************
 * Function         : myShell
 *
 * Description      : This section of code displays user defined shell prompt.
 *                    It reads command from the command line and calls different
 *                    functions depending on the command typed on the prompt.
 *
 * Input Argument   : void
 *
 * Output Argument  : None
 *
 * Return Value     : None
 *****************************************************************************/
int myShell(void)
{
   int res = TRUE, j = 0, piped = FALSE, parallel = FALSE;
   profile* profile = NULL;
   char** argmt = NULL;
   char *name/*[COMMAND_LENGTH]*/;
   char pipe_command[COMMAND_LENGTH];
   char* tokens[MAXIMUM_TOKENS];
   char* retu_token[MAXIMUM_TOKENS];
   char* tokens1[MAXIMUM_TOKENS];
   int name_len = 0;
   int iID = 0;
   history *pTemp = NULL;
   char *pTempName = NULL;
   int iNumber = 0;

   name = (char *)malloc(COMMAND_LENGTH*sizeof(char));

   ConfigurePrfl(&profile);
   gotoHome(profile);
   signal(SIGINT, ctrl_CHandler);
   setjmp(jump_buffer);
   printf("%s: ",profile->prompt);
	
   while (fgets(name, COMMAND_LENGTH, stdin) != NULL)
   {
      iID++;
      name_len = strlen(name);
      pTempName = (char*)malloc (COMMAND_LENGTH*sizeof(char));
      strcpy (pTempName, name);

      get_history(pTempName, name_len,iID);

      strcpy(pipe_command, "");
      strcpy(pipe_command,name);
      getTokens(name,tokens);

/*printf("tokens[0]:%s\n",tokens[0]);
printf("tokens[1]:%s\n",tokens[1]);
printf("tokens[2]:%s\n",tokens[2]);
printf("tokens[3]:%s\n",tokens[3]);
  */                  
      if(*tokens != NULL)
	    {
         if (strcmp(tokens[0], "exit") == 0) 
		     {
		        exit(0);
		     }
         else if (strcmp(tokens[0], "PROMPT") == 0) 
         {  
		        strcpy(profile->prompt,tokens[1]);
         }
		     else if (strcmp(tokens[0], "HOME") == 0)
         {
		        strcpy(profile->home,tokens[1]);
            gotoHome(profile);
		     }		
         else if((strcmp(tokens[0], "cd") == 0) || (strcmp(tokens[0], "chdir") == 0))
         {  
            res =  chdir(tokens[1]);
            if(res != 0)
            {
		           printf("ERROR: Not able to move into another directory\n");
               return FALSE; 
            } 
		     }
	       else if ((strcmp(tokens[0], "getpid") == 0)||(strcmp(tokens[0],"GETPID")==0))
		     {         
		        spawn_process(1);
		     }
		     else if ((strcmp(tokens[1], "=") == 0)&& (strcmp(tokens[4], "=") == 0) && (strcmp(tokens[0],"PROMPT")!=0)&&(strcmp(tokens[0],"HOME")!=0)&&(strcmp(tokens[0],"home")!=0)&&(strcmp(tokens[0],"prompt")!=0))
         {     
            concat_commnd(tokens);
 	     	 } 
         else if ((strcmp(tokens[1], "=") == 0)&& (strcmp(tokens[4], "=") == 0 ) && (strcmp(tokens[7], "=") == 0 )&& (strcmp(tokens[0],"PROMPT")!=0)&&(strcmp(tokens[0],"HOME")!=0)&&(strcmp(tokens[0],"home")!=0)&&(strcmp(tokens[0],"prompt")!=0))
         {     
            concat_commnd(tokens);
 	     	 } 
 	     	 else if (((strcmp(tokens[0], "printenv") == 0) || (strcmp(tokens[0], "setenv") == 0) || (strcmp(tokens[0], "echo") == 0) || (strcmp(tokens[0], "pwd") == 0)) && (strcmp(tokens[0],"PROMPT")!=0)&&(strcmp(tokens[0],"HOME")!=0)&&(strcmp(tokens[0],"home")!=0)&&(strcmp(tokens[0],"prompt")!=0))
         {     
            char* j = tokens[2];     		
            executeCommand(tokens,parallel);
         }
         else if ((strcmp(tokens[0],"Alias")==0))
         {
            char tempToken1[256];
			      char tempToken2[256];

			      memset(tempToken1,'\0',256);
			      memset(tempToken2, '\0',256);

			      strcpy (tempToken1,tokens[1]);
			      strcpy (tempToken2,tokens[3]);
 
			      printf("@:");
			      fgets(name, COMMAND_LENGTH,stdin);
				
			      getTokens(name,tokens);
			      if ((strcmp(tokens[0],tempToken1)==0) || (strcmp(tokens[0],tempToken2)==0))
			      {
               strcpy(tokens[0],tempToken1);
				       executeCommand(tokens,parallel);
				       strcpy(tokens[0],tempToken2);
				       executeCommand(tokens,parallel);
			      }
			      memset(tokens,'\0',8);
			    }
          else if ((strcmp(tokens[0], "ls")==0) && !(strcmp(tokens[2], ">")==0))
          { 
             executeCommand(tokens,parallel);
          } 
          else if (strcmp(tokens[0], "cat")==0 && !(strcmp(tokens[2],"|") == 0))
          {
             if(tokens[1]==NULL)
                printf("\nThe file does not exist\n");
             else 
                RunFnct(tokens);
          }
          else
	        { 
             piped = FALSE;
             for (j=0; name[j] != '\n' && j<name_len && !piped; j++)
	           {
	              if (name[j] == '|') 
	              {
		               piped = TRUE;
	              }
             }
             redirection_type = 2;
             /*printf("value is %d",redirectionType);*/
             for(j = 0; name[j] != '\n' && j<name_len && (redirection_type!=1); j++)
             {
                if(name[j] == '>')
                {
                   redirection_type = 1;
                   /* printf("value is %d",redirection_type);*/
                }
             }
             if(piped)
             {
/*printf("****Shyam\n");   
                     	   printf("\n Pipe command\n");
*/	              exePipeCommand(pipe_command, parallel);
             }	
             else if(redirection_type == 1)
             {
                run_redirected(pipe_command,tokens,redirection_type);
             }     
             else
             {  
		            iNumber = 0;
		            for(pTemp = pStart; NULL != pTemp; pTemp = pTemp->pNextAddress)
		            {
		               iNumber++;
		               if (NULL != (pTemp->pAlias))
	                 {
		                  if (strcmp(tokens[0], (pTemp->pAlias))==0)
		                  {
 		                     if (NULL != pTemp->pAliasCmd)
			                   {
			                      strcpy(tokens[0],(pTemp->pAliasCmd));
			                      break;
			                   }
		                  }
		               }
                }

		            if('!' == name[0])
		            {
		               if('!' == name[1])
		               {
		                  for(pTemp=pStart;pTemp!=NULL;pTemp=pTemp->pNextAddress)
		                  {
			                   if((iNumber-1) == (pTemp->iID))
			                   {
			                      strcpy(name,pTemp->pLine);
			                      getTokens(name,tokens);
			                   }
		                  }
		               }
		               else if('-' == name[1])
		               {
		                  int i = 2, j = 0;
		                  char iNumArray[100];
		                  int iFinalVal = 0;

		                  while(name[i] != '\0')
		                  {
			                   iNumArray[j] = name[i];
			                   j++;
			                   i++;
		                  }

		                  iNumArray[j] = '\0';
		                  iFinalVal = atoi(iNumArray);

		                  for(pTemp=pStart;pTemp!=NULL;pTemp=pTemp->pNextAddress)
		                  {
			                   if((iNumber-iFinalVal-1)==(pTemp->iID))
			                   {
			                      strcpy(name,(pTemp->pLine));
			                      getTokens(name,tokens);
			                   }
			                   if((iNumber-iFinalVal-1) <= 0)
			                   {
			                      printf("No command available...\n");
			                      break;
			                   }
		                  }
		               }
		               else
			                printf("Wrong Command !!!!\n");
		            }
		                  
                executeCommand(tokens, parallel);
             }
	        }
       }

       BufferClear(name, COMMAND_LENGTH);
       BufferClear(pipe_command, COMMAND_LENGTH);
       printf("%s: ",profile->prompt);
    }
    return res;
 }


/******************************************************************************
 * Function         : main
 *
 * Description      : This section of code is the main class which gives a
 *                    call to the myShell.
 *
 * Input Argument   : void
 *
 * Output Argument  : None
 *
 * Return Value     : None
 *****************************************************************************/
int main (int argc, char** arg)
{       
        system("clear");
        printf("********************************\n");
        printf("Welcome to Shell Developed By:\n");
        printf("Shyam Gajjar        - A20244964\n");
        printf("Aarthi Ashok        - A20242389\n");
        printf("Radhika Ramamoorthy - A20243303\n");
        printf("********************************\n");
	myShell();
}
