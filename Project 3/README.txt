﻿

*********************************************************************

README FOR Filesystem program
*********************************************************************
Team Members:
----------------
VIVEK VISWANATHAN(A20294994)
KINNERA RAVULAPALLI(A20326163)
SAHITYA VANTIPALLI(A20326237)


Minix Version : 3.2.1 
Compiler : clang 

User name:root
Password: root123

Loading Program to minix:
------------------------------

1. use the below commands to configure ssh
	pkgin update
	pkgin install openssh
2. Set the password for user by typing passwd
3. Use an ftp client like fileZilla to connect to MINIX using ssh and transfer the file 
4. Naviagate to /Project3/
5. Run the program FsTool
6. Please select between inodewalker,zonewalker,erase inodes and resotre inodes
7. Source code is /usr/src/servers/vs and /usr/src/servers/mfs folder
8. A copy of all sources is given in Source Code folder.
9. Document folder would contain the design document and test cases
