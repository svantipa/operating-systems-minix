#include "fs.h"
#include <fcntl.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>
#include <minix/u64.h>
#include <minix/keymap.h>
#include <sys/ptrace.h>
#include <sys/svrctl.h>
#include <minix/com.h>
#include "file.h"
#include "fproc.h"
#include "vnode.h"
#include "vmnt.h"
#include "param.h"
#include "path.h"
#include "scratchpad.h"
#include "dmap.h"
#include <minix/vfsif.h>
#include <minix/callnr.h>
#include <minix/safecopies.h>
#include <minix/endpoint.h>
#include <minix/com.h>
#include <minix/sysinfo.h>


int do_inodewalker(){
  struct vnode *vp;
  struct vmnt *vmp;
  struct lookup resolve;
  char *devPath;
  devPath="/dev/c0d0p0s1";

  lookup_init(&resolve, devPath, PATH_NOFLAGS, &vmp, &vp);
  resolve.l_vmnt_lock = VMNT_READ;
  resolve.l_vnode_lock = VNODE_READ;

  if ((vp = eat_path(&resolve, fp)) == NULL) {
				printf("unknown MFS API error\n");
				return -1;
   }
   	req_inodewalker(vp->v_fs_e, 20);
  return 0;
}


int do_zonewalker(){

  int inode_nr;
  inode_nr=m_in.m3_i1;
  struct vnode *vp;
  struct vmnt *vmp;
  char *devPath;
  struct lookup resolve;
  
  devPath="/dev/c0d0p0s1";
  lookup_init(&resolve, devPath, PATH_NOFLAGS, &vmp, &vp);
  resolve.l_vmnt_lock = VMNT_READ;
  resolve.l_vnode_lock = VNODE_READ;

  if ((vp = eat_path(&resolve, fp)) == NULL) {
  
  printf("unknown MFS API error\n");
				return -1;
  
  }
  req_zonewalker(vp->v_fs_e, inode_nr);
  return 0;

}
int do_eraseinode(){

	int inode_nr;
	inode_nr=m_in.m3_i1;

	struct vnode *vp;
	struct vmnt *vmp;
	char *devPath;
	struct lookup resolve;
	devPath="/dev/c0d0p0s1";
	lookup_init(&resolve, devPath, PATH_NOFLAGS, &vmp, &vp);
	
	resolve.l_vmnt_lock = VMNT_READ;
	resolve.l_vnode_lock = VNODE_READ;
 
  if ((vp = eat_path(&resolve, fp)) == NULL) {
				printf("unknown MFS API error\n");
				return -1;
    }

  req_eraseinode(vp->v_fs_e, inode_nr);

  return 0;

}
int do_restorefile(){

	int inode_nr;
	inode_nr=m_in.m3_i1;

	struct vnode *vp;
	struct vmnt *vmp;
	char *devPath;
	struct lookup resolve;
	devPath="/dev/c0d0p0s1";
	lookup_init(&resolve, devPath, PATH_NOFLAGS, &vmp, &vp);
	resolve.l_vmnt_lock = VMNT_READ;
	resolve.l_vnode_lock = VNODE_READ;
  
	if ((vp = eat_path(&resolve, fp)) == NULL) {
		printf("unknown MFS API error\n");
		return -1;
	}
  	
	req_restorefile(vp->v_fs_e, inode_nr);
  return 0;
}
