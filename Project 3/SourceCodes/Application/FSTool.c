#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <lib.h>
#include <fcntl.h>
#include <unistd.h>
#include<sys/wait.h>

message m;

int inodeWalker(){
    return(_syscall(VFS_PROC_NR, 68, &m)); 
}

int zoneWalker(){
   return(_syscall(VFS_PROC_NR, 69, &m)); 
}

int eraseInode( int inode_nr){
     message m;
     m.m3_i1 = inode_nr;
    return(_syscall(VFS_PROC_NR, 70, &m)); 
}

int restoreFile(int inode_nr){
     message m;
     m.m3_i1 = inode_nr;
    return(_syscall(VFS_PROC_NR, 79, &m)); 
}

int main(){
	int input=0,inodeNum=0;
		
    printf("\n*********************File system tools*********************\n");
	do{
		printf("\nPlease enter 1 for Inode walker, 2 for Zonemapwalker, 3 for Erasing a Inode, 4 for Restoring an Inode and 0 for exit\n");
		 scanf("%d",&input);
		 
		  switch(input){
          
              case 1: 
			  inodeWalker();
              break;
                      
              case 2: 
			  zoneWalker();
              break;
                      
              case 3: 
			  printf("Please input the Inode Number to Erase : ");
			  scanf("%d",&inodeNum); 
			  eraseInode(inodeNum);
			  printf("\n");
              break;
					  
			  case 4: 
			  printf("\nPlease input the Inode Number to Restore : ");
			  scanf("%d",&inodeNum); 
			  restoreFile(inodeNum);
			  printf("\n");
              break;
					  
             case 0: 
			 exit(0);
             break;
			 
			 default:
			 break;
          		 
		}	
	}while(input !=0);    
}
